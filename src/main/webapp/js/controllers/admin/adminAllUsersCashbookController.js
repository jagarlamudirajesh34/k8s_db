samvit.controller("adminAllUsersCashbookController", [
		'$scope',
		'$route',
		'$http',
		'$rootScope',
		'OAuth',
		'$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/admin/adminHeader.html";
			
			$scope.pageInit = function(){
				$http.get('main/getAllActiveUsersCashbook').success(
					function(response, status, headers) {
						console.log(response);
						if (response.status == 'success') {
							$scope.allUsersCashBook = response.result;
						} else {
							$scope.allUsersCashBook = null;
						}
				});
			};
			
		}]);