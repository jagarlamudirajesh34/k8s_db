samvit.controller("adminAllBankTransactionsController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				function($scope, $route, $http, $rootScope, OAuth,$location){
					$scope.headerTemplate = "views/admin/adminHeader.html";
					//fetch all Users Details
					$scope.pageInit = function(){
						$http.get('bankTransaction/getAllBankTransactions').success(
							function(response, status, headers) {
								console.log(response);
								if (response.status == 'success') {
									$scope.allBankTran = response.result;
								} else {
									$scope.allBankTran = null;
								}
						});
					};
					
				}]);