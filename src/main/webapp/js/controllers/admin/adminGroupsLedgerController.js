samvit.controller("adminGroupsLedgerController", [
		'$scope',
		'$route',
		'$http',
		'$rootScope',
		'OAuth',
		'$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/admin/adminHeader.html";
			// fetch all Users Details
			$scope.pageInit = function() {
				$scope.listGroupsView = true;
				$scope.addGroupsView = false;
				$scope.upload = true;
				$scope.preview = true;
				$http.get('groupLedger/getAllActiveTopGroups').success(
						function(response, status, headers) {
							if (response.status == 'success') {
								$scope.getAllActiveTopGroups = response.result;
							} else {
								$scope.getAllActiveTopGroups = null;
							}
						});

			};

			$scope.addGroupButtonForAdding = function() {
				$scope.listGroupsView = false;
				$scope.addGroupsView = true;
			};

			$scope.cancelButton = function() {
				$scope.listGroupsView = true;
				$scope.addGroupsView = false;
				$scope.pageInit();
			}

			$scope.saveButtonGroup = function() {
				alert(1);
			};

			$scope.getLiabilityMainGroups = function() {
				$http.get('groupLedger/getAllLiabilityMainGroups').success(
						function(response, status, headers) {
							console.log(response);
							if (response.status == 'success') {
								$scope.getAllLiabilityMainGroups = response.result;
							} else {
								$scope.getAllLiabilityMainGroups = null;
							}
						});
			};
			
			$scope.getSubGroups = function(lg){
				console.log(lg);
				$scope.checkService();
				$http.get('groupLedger/getSubGroups/'+lg.id).success(
						function(response, status, headers) {
							console.log(response);
							if (response.status == 'success') {
								$scope.getSubGroups = response.result;
							} else {
								$scope.getSubGroups = null;
							}
						});
			};
			
			$scope.checkService = function(){
				console.log("check");
				var d = new Date();
				d.setDate(1);
				d.setMonth(1);
				console.log(d);
				$http.get('main/getAllCashbookTransactionsBasedOnCashbookIdDateStatus/1/'+d+'/'+new Date()+'/rejected').success(
						function(response, status, headers) {
							console.log(response);
						}
				)}
			
			$scope.addDriverButton = function(){
				alert(1);
				console.log(new Date());
				var bankTransaction = {
					transactionDate: new Date(),
					description: "testing",
					credit: 1,
					debit: 0,
					balance: 1,
					bank : {
						id: 1
					},
					party: "abc"						
				}
				console.log(bankTransaction);
				var formData = new FormData();
				formData.append("bankTransaction", angular.toJson(bankTransaction, true));
				if($scope.profilePic != null){
					formData.append("paymentSlipPic", $scope.profilePic);
				}
				formData.append("transactionDate", "10-05-2020");
				$http({	url : 'bankTransaction/addTransaction',
							method : 'POST',
							data : formData,
							transformRequest : function(data,headersGetterFunction) {
								return data; 
							},
							headers : {
								'Content-Type' : undefined,
								'Accept' : 'application/json'
							}
						})
						.then(
								function(resp) {
									console.log(resp);
									//$scope.initializePage();
								});
				
				
			}
			
			
		} ]);