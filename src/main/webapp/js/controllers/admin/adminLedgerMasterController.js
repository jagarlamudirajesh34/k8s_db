samvit.controller("adminLedgerMasterController",[
				'$scope',
				'$route',
				'$http',
				'$rootScope',
				'OAuth',
				'$location',
				function($scope, $route, $http, $rootScope, OAuth,$location){
					$scope.headerTemplate = "views/admin/adminHeader.html";
					$scope.gridOptions = {};
					String.prototype.isNumber = function(){return /^\d+$/.test(this);}
					$scope.namePattern = /^[a-zA-Z]+$/; 
					$scope.emailPattern =  /^[A-Za-z]+[A-Za-z0-9._]+@[A-Za-z]+\.[A-Za-z.]{2,5}$/;
					$scope.upperCase = /^[^A-Z]+$/;
					$scope.grpId = "";
					$scope.lmid = "";
					$scope.a = true;
					$scope.b = true;
					$scope.c = true;
					$scope.d = true;
					$scope.e = true;
					$scope.f = true;
					$scope.g = true;
					
					//fetch all Active Ledgers
					$http.get('groupLedger/getAllActiveLedgersForLedgerMaster').success(
							function(response, status, headers) {
								if (response.status == 'success') {
									$scope.allActiveLedgers = response.result;
									$scope.gridOptions.rowHeight = 48;
									$scope.gridOptions.data = response.result;     // BIND JSON TO GRID.
              					    $scope.gridOptions.columnDefs = [
                                          { field: 'groupName', displayName: 'Ledger Name',type: 'string', enableCellEdit: true, maxWidth: 120},
                                          { field: 'ledgerMasterInfo', displayName: 'Master Data', enableCellEdit: false, cellTemplate: '<div style=""><span ng-bind-html="row.entity[col.field]"></span><button style="color:#f7f7ad; background: #42210b;" ng-click="grid.appScope.sampledetails(row)">Edit</button></div>'},
                                          { field: 'debit', displayName: 'Balance',type: 'number', enableCellEdit: false, maxWidth: 100},
                                          { field: 'groupHierarchyName', displayName: 'General Group',type: 'string', enableCellEdit: false, maxWidth: 300},
                                          { field: 'debitLdegerName', displayName: 'ROC Group',type: 'string', maxWidth: 300}
                                          ];
								} else {
									$scope.gridOptions.data = [];
									$scope.allActiveLedgers = null;
								}
						});
					
					//for search operation
					$scope.callToSearch = function(){
						if($scope.search == null || $scope.search == "" || $scope.search == undefined){
							$http.get('groupLedger/getAllActiveLedgersForLedgerMaster').success(
									function(response, status, headers) {
										if (response.status == 'success') {
											$scope.allActiveLedgers = response.result;
											$scope.gridOptions.data = response.result; 
										}else{
											$scope.allActiveLedgers = null;
											$scope.gridOptions.data = []; 
										}
									});
						}else{
							$http.get('groupLedger/searchOnLedgerMasterData/'+$scope.search).success(
									function(response, status, headers) {
										if (response.status == 'success') {
											console.log( response.result);
											$scope.allActiveLedgers = response.result;
											$scope.gridOptions.data = response.result;  
										}else{
											$scope.allActiveLedgers = null;
											$scope.gridOptions.data = [];
										}
						  });
						}
					}
						
						
						
					
					//for entering master data call
					$scope.sampledetails = function(g){
						$scope.grpId = g.entity.id;
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.id != null){
						    $scope.lmid = g.entity.ledgerMasterData.id;
						}		
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.name != null){
						    $scope.name = g.entity.ledgerMasterData.name;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.state != null){
							$scope.state = g.entity.ledgerMasterData.state;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.pincode != null){
							$scope.pincode = g.entity.ledgerMasterData.pincode;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.email != null){
							$scope.mail = g.entity.ledgerMasterData.email;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.phone != null){
							$scope.phone = g.entity.ledgerMasterData.phone;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.aadhar != null){
							$scope.aadhar = g.entity.ledgerMasterData.aadhar;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.pan != null){
							$scope.pan = g.entity.ledgerMasterData.pan;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.pincode != null){
							$scope.pincode = g.entity.ledgerMasterData.pincode;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.gstNumber != null){
							$scope.gst = g.entity.ledgerMasterData.gstNumber;
						}
						if(g.entity.ledgerMasterData != null && g.entity.ledgerMasterData.address != null){
							$scope.address = g.entity.ledgerMasterData.address;
						}
						var element = angular.element('#exampleModalCenter');
						element.modal('show');
					};
					
					
					
					//for saving master data call
					$scope.saveChanges = function(){
						$scope.checkName();
						$scope.gr = {
						  id: $scope.grpId,
  						  ledgerMasterData : {
  							id: $scope.lmid,
							name: $scope.name,
							address: $scope.address,
							state: $scope.state,
							pincode: $scope.pincode,
							email: $scope.mail,
							phone: $scope.phone,
							aadhar: $scope.aadhar,
							pan: $scope.pan,
							gstNumber: $scope.gst				
						 }
						};
						if($scope.a == true && $scope.b == true && $scope.c == true && $scope.d == true && $scope.e == true && $scope.f == true && $scope.g == true){
							$http({url : "groupLedger/editLedgerMasterData",
          						headers : {
          									'Content-Type' : 'application/json',
          									'Accept' : 'application/json'
          								},
          						method : "POST",
          						data : $scope.gr
          						}).success(function(returnData) {
          						if (returnData.status == "success") {
          						   var element = angular.element('#exampleModalCenter');
         						   element.modal('hide');
         						   $scope.cancel();
         						   $scope.callToSearch();
         							
         						   /*$http.get('groupLedger/getAllActiveLedgersForLedgerMaster').success(
         									function(response, status, headers) {
         										if (response.status == 'success') {
         											$scope.allActiveLedgers = response.result;
         											$scope.gridOptions.data = response.result;     // BIND JSON TO GRID.
         										} else {
         											$scope.gridOptions.data = [];
         											$scope.allActiveLedgers = null;
         										}
         								});*/
          						}else{
          						}
          					});
							
							
						}else{
						}

					};
					
					$scope.cancel = function(){
						$scope.grpId = "";
						$scope.lmid = "";
						
						$scope.name = "";
						$scope.state = "";
						$scope.pincode = "";
						$scope.phone = "";
						$scope.aadhar = "";
						$scope.mail = "";
						$scope.pan = "";
						$scope.gst = "";
						$scope.address = "";
						
						$scope.nameError = false;
						$scope.pincodeError = false;
						$scope.phoneError = false;
						$scope.aadharError = false;
						$scope.mailError = false;
						$scope.panError = false;
						$scope.gstError = false;
					};
					
					
					//for checking name or not
					$scope.checkName = function(){
						if($scope.name == "" || $scope.name == null || $scope.name == undefined){
							$scope.a = false;
							$scope.nameError = true;
							$scope.nameErrorMessage = "Enter name";
						}else{
							$scope.a = true;
							$scope.nameError = false;
							$scope.nameErrorMessage = "";	
						}
					};
					
					//for checking pincode number or not
					$scope.checkPincode = function(){
						if($scope.pincode == "" || $scope.pincode == null || $scope.pincode == undefined){
							$scope.b = true;
							$scope.pincodeError = false;
							$scope.pincodeErrorMessage = "";
						}else if(($scope.pincode != "" || $scope.pincode != null || $scope.pincode != undefined) && ($scope.pincode.length != 6 || !$scope.pincode.isNumber())){
							$scope.b = false;
							$scope.pincodeError = true;
							$scope.pincodeErrorMessage = "Enter only digits and length is 6";
						}else{
							$scope.b = true;
							$scope.pincodeError = false;
							$scope.pincodeErrorMessage = "";	
						}
					};
					
					//for checking phone number number or not
					$scope.checkPhone = function(){
						if($scope.phone == "" || $scope.phone == null || $scope.phone == undefined){
							$scope.c = true;
							$scope.phoneError = false;
							$scope.phoneErrorMessage = "";
						}else if(($scope.phone != "" || $scope.phone != null || $scope.phone != undefined) && ($scope.phone.length != 10 || !$scope.phone.isNumber())){
							$scope.c = false;
							$scope.phoneError = true;
							$scope.phoneErrorMessage = "Enter only digits and length is 10";
						}else{
							$scope.c = true;
							$scope.phoneError = false;
							$scope.phoneErrorMessage = "";	
						}
					}
					
					//for checking Aadhar number or not
					$scope.checkAadhar = function(){
						if($scope.aadhar == "" || $scope.aadhar == null || $scope.aadhar == undefined){
							$scope.d = true;
							$scope.aadharError = false;
							$scope.aadharErrorMessage = "";
						}else if(($scope.aadhar != "" || $scope.aadhar != null || $scope.aadhar != undefined) && ($scope.aadhar.length != 12 || !$scope.aadhar.isNumber())){
							$scope.d = false;
							$scope.aadharError = true;
							$scope.aadharErrorMessage = "Enter only digits and length is 12";
						}else{
							$scope.d = true;
							$scope.aadharError = false;
							$scope.aadharErrorMessage = "";	
						}
					};
					
					//for checking mail forma
					$scope.checkMail = function(){
						if($scope.mail == "" || $scope.mail == null || $scope.mail == undefined){
							$scope.e = true;
							$scope.mailError = false;
							$scope.mailErrorMessage = "";
						}else if(($scope.mail != "" || $scope.mail != null || $scope.mail != undefined) && !$scope.emailPattern.test($scope.mail)){
							$scope.e = false;
							$scope.mailError = true;
							$scope.mailErrorMessage = "Enter vali email format";
						}else{
							$scope.e = true;
							$scope.mailError = false;
							$scope.mailErrorMessage = "";	
						}
					};
					
					$scope.checkPan = function(){
						$scope.pan = $scope.pan.toUpperCase();
						if($scope.pan == "" || $scope.pan == null || $scope.pan == undefined){
							$scope.f = true;
							$scope.panError = false;
							$scope.panErrorMessage = "";
						}else if(($scope.pan != "" || $scope.pan != null || $scope.pan != undefined) && $scope.pan.length != 10){
							$scope.f = false;
							$scope.panError = true;
							$scope.panErrorMessage = "Please enter valid pan card";
						}else if(($scope.pan != "" || $scope.pan != null || $scope.pan != undefined) && /[^a-z]/i.test($scope.pan.substring(0,5))){
							$scope.f = false;
							$scope.panError = true;
						    $scope.panErrorMessage = "Please enter charecters from 1 - 5 postions. eg: ABCDE1234F";
						}else if(($scope.pan != "" || $scope.pan != null || $scope.pan != undefined) && !$scope.pan.substring(5,9).isNumber()){
							$scope.f = false;
							$scope.panError = true;
						    $scope.panErrorMessage = "Please enter numbers from 6 - 9 postions. eg: ABCDE1234F";
						}else if(($scope.pan != "" || $scope.pan != null || $scope.pan != undefined) && $scope.upperCase.test($scope.pan.substring(9,10))){
							$scope.f = false;
							$scope.panError = true;
						    $scope.panErrorMessage = "Please enter charecter in 10th position. eg: ABCDE1234F";
						}else{
							$scope.f = true;
							$scope.panError = false;
							$scope.panErrorMessage = "";	
						}
					}
					
					//for checking GST
					$scope.checkGST = function(){
						$scope.gst = $scope.gst.toUpperCase();
						if($scope.gst == "" || $scope.gst == null || $scope.gst == undefined){
							$scope.g = true;
							$scope.gstError = false;
							$scope.gstErrorMessage = "";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) && $scope.gst.length != 15){
							$scope.g = false;
							$scope.gstError = true;
							$scope.gstErrorMessage = "Please enter valid 15 digit GST Number";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) &&  !$scope.gst.substring(0,2).isNumber()){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please enter first 2 digists only. eg:01";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) && $scope.gst.substring(0,2).isNumber() && ($scope.gst.substring(0,2) < 01 || $scope.gst.substring(0,2) > 35)){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please enter state code 01 - 35";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) && $scope.gst.substring(2,12) !== $scope.pan){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please PAN and GST numbers";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) &&  !$scope.gst.charAt(12).isNumber()){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please enter 13th position as number";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) &&  !$scope.gst.charAt(14).isNumber()){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please enter 15th position as number";
						}else if(($scope.gst != "" || $scope.gst != null || $scope.gst != undefined) &&  !$scope.gst.charAt(13) === "Z"){
							$scope.g = false;
							$scope.gstError = true;
						    $scope.gstErrorMessage = "Please enter 14th position as charecter 'Z'";
						}else{
							$scope.g = true;
							$scope.gstError = false;
							$scope.gstErrorMessage = "";	
						}
					};
					
					
				}])