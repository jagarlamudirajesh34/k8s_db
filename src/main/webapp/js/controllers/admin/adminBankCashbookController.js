samvit.controller("adminBankCashbookController", [
		'$scope',
		'$route',
		'$http',
		'$rootScope',
		'OAuth',
		'$location',
		function($scope, $route, $http, $rootScope, OAuth, $location) {
			$scope.headerTemplate = "views/admin/adminHeader.html";
			// fetch all Users Details
			$scope.pageInit = function() {
				$http.get('bank/getAllBanksCashbooks').success(
						function(response, status, headers) {
							console.log(response);
							if (response.status == 'success') {
								$scope.allBankTran = response.result;
							} else {
								$scope.allBankTran = null;
							}
						});

			};

			$scope.allCall = function() {
				$scope.pageInit();
			}
			$scope.bankCall = function() {
				$scope.allBankTran = null;
				$http.get('bank/getAllBanks/true').success(
						function(response, status, headers) {
							console.log(response);
							if (response.status == 'success') {
								$scope.allBankTran = response.result;
							} else {
								$scope.allBankTran = null;
							}
						});
			}
			$scope.cashBookCall = function() {
				$scope.allBankTran = null;
				$http.get('bank/getAllBanks/false').success(
						function(response, status, headers) {
							console.log(response);
							if (response.status == 'success') {
								$scope.allBankTran = response.result;
							} else {
								$scope.allBankTran = null;
							}
						});
			}
		} ]);
