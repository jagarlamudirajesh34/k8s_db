samvit.directive('uiSelectWrap', uiSelectWrap);
samvit.controller("testController", [ '$scope', '$route', '$http',
                              		'$rootScope', 'OAuth', '$location',
                              		function($scope, $route, $http, $rootScope, OAuth, $location) {
                              			$scope.headerTemplate = "views/admin/adminHeader.html";
                              			$scope.gridOptions = {};
                              			$scope.showDateMessage = false;
                              			$scope.allActiveLedger = null;
                              			$http.get('groupLedger/getAllActiveLedgers').success(
                  	    						function(response, status, headers) {
                  	    							$scope.allActiveLedger = null;
                  	    							if (response.status == 'success') {
                  	    								console.log(response.result);
                  	    								$scope.allActiveLedger = response.result;
                  	    							} else {
                  	    							    $scope.allActiveLedger = null;
                  	    							}
                  	    						});
                              			
                              			/*
                              			$('.datepicker').datepicker({
                              		         endDate: new Date(),
                              		         autoclose: true
                              		     });
                              			
                              			$('.toDatepicker').datepicker({
                              		         endDate: new Date(),
                              		         autoclose: true
                              		     });
                              			*/
                              			
                              			$scope.test = ['male', 'female', 'others'];
                              			$scope.changeFromDate = function(){
                              				$scope.changeToDate();
                              			};
                              			
                              			$scope.changeToDate = function(){
                              				$scope.showDateMessage = false;
                              				if(new Date($scope.fromDate) <= new Date($scope.toDate)){
                              					var fDate = new Date($scope.fromDate);
                              					var dd = String(fDate.getDate()).padStart(2, '0');
                              					var mm = String(fDate.getMonth() + 1).padStart(2, '0');
                              					fDate = dd + '-' + mm + '-' + fDate.getFullYear();
                              					
                              					var tDate = new Date($scope.toDate);
                              					var dd = String(tDate.getDate()).padStart(2, '0');
                              					var mm = String(tDate.getMonth() + 1).padStart(2, '0');
                              					tDate = dd + '-' + mm + '-' + tDate.getFullYear();	
                              					
                              					var request1 = {
                              				             method: 'get',
                              				             url: 'http://casoftware-env.eba-fp44swnf.ap-south-1.elasticbeanstalk.com/bankTransaction/getAllBankTransactionsForTableView/'+fDate+'/'+tDate,
                              				             dataType: 'json',
                              				             contentType: "application/json"
                              				         };
                              					$http(request1)
                              		            .success(function (jsonData) {
                              					    $scope.gridOptions.data = jsonData.result;     // BIND JSON TO GRID.
                              					    $scope.gridOptions.columnDefs = [
                                                          { field: 'transactionId', displayName: 'Rec No',type: 'int', maxWidth: '90', enableCellEdit: false},
                                                          { field: 'transactionDate', displayName: 'Date',type: 'date', cellFilter: 'date:\'dd-MM-yyyy\'', maxWidth: '90', enableCellEdit: false},
                                                          { field: 'debit', displayName: 'Outflow',type: 'int', maxWidth: '90', enableCellEdit: false},
                                                          { field: 'credit', displayName: 'Inflow',type: 'int', maxWidth: '80', enableCellEdit: false},
                                                          { field: 'debitLdegerName', displayName: 'Debit Ledger',type: 'string', maxWidth: '130'},
                                                          { field: 'creditLedgerName', displayName: 'Credit Ledger',type: 'string', maxWidth: '130'},
                                                          { field: 'bankName', displayName: 'Bank Name',type: 'String', maxWidth: '150', enableCellEdit: false},
                                                          { field: 'userComments', displayName: 'Comments',type: 'String'},
                                                          ];
                              					    //$scope.gridOptions.enableColumnResize = true;
                              					    //$scope.gridOptions.maxWidth = 80;
                              		            })
                              		            .error(function () { });
                              				}else{
                              					$scope.dateMessage = "To-date should be after From-Date";
                              					$scope.showDateMessage = true;
                              				}
                              			};
                              			
                              	         var request = {
                              	             method: 'get',
                              	             url: 'http://casoftware-env.eba-fp44swnf.ap-south-1.elasticbeanstalk.com/bankTransaction/getAllBankTransactionsForTableView',
                              	             dataType: 'json',
                              	             contentType: "application/json"
                              	         };
                                           
                              	         
                              	         $http(request)
                              	            .success(function (jsonData) {
                              	                $scope.gridOptions.data = jsonData.result;     // BIND JSON TO GRID.
                              	                console.log(jsonData.result);
                              	                $scope.gridOptions.rowHeight = 38;
                              	                $scope.gridOptions.columnDefs = [
                              									{ field: 'transactionId', displayName: 'Rec No',type: 'int', enableCellEdit: false, maxWidth: 90},
                              									{ field: 'transactionDate', displayName: 'Date',type: 'date', cellFilter: 'date:\'dd-MM-yyyy\'', enableCellEdit: false, maxWidth: 90},
                              									{ field: 'debit', displayName: 'Outflow',type: 'int', enableCellEdit: false, maxWidth: 90},
                              									{ field: 'credit', displayName: 'Inflow',type: 'int', enableCellEdit: false, maxWidth: 80},
                              									{ field: 'debitLedgerName', displayName: 'Debit Ledger', maxWidth: 140, enableCellEdit: true, editableCellTemplate: 'uiSelect', editDropdownOptionsArray: $scope.allActiveLedger},
                              									{ field: 'creditLedgerName', displayName: 'Credit Ledger', maxWidth:140, enableCellEdit: true, editableCellTemplate: 'uiSelect', editDropdownOptionsArray: $scope.allActiveLedger },
                              									{ field: 'description', displayName: 'Description',type: 'String', enableCellEdit: false, cellTemplate: '<div style=""><span ng-bind-html="row.entity[col.field]"></span></div>'},
                              									{ field: 'userComments', displayName: 'Comments',type: 'String'},
                              									];
                              				    //$scope.gridOptions.enableColumnResize = true;
                              				    //$scope.gridOptions.maxWidth = 80;
                              	            })
                              	            .error(function () { });
                              	         
                              	         $scope.gridOptions.onRegisterApi = function(gridApi) {
                              	        	  $scope.gridApi = gridApi;
                              	        	  
                              	        	gridApi.cellNav.on.navigate($scope,function(newRowCol, oldRowCol) {
                              	              if(oldRowCol.col.colDef.field == "creditLedgerName" || oldRowCol.col.colDef.field == "debitLedgerName"){
                              	            	var bt = {
                              	            		transactionId: oldRowCol.row.entity.transactionId,
                              	            		creditLedgerName: oldRowCol.row.entity.creditLedgerName,
                              	            		debitLedgerName: oldRowCol.row.entity.debitLedgerName,
                              	            		creditLedgerId: oldRowCol.row.entity.debitLedgerId,
                              	            		debitLedgerId: oldRowCol.row.entity.creditLedgerId
                              	            	}
                              	            	$http({url : "bankTransaction/updateLedgersInTransactioninUiGrid",
                              						headers : {
                              									'Content-Type' : 'application/json',
                              									'Accept' : 'application/json'
                              								},
                              						method : "POST",
                              						data : bt
                              					}).success(function(returnData) {
                              						if (returnData.status == "success") {
                              						}else{
                              						}
                              					});
                              	              }
                              	           });
                              	        	gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
                              	        	    var transactionDto = {
                              	        	    		transactionId: 	rowEntity.transactionId,
                              	        	    		description: rowEntity.description,
                              	        	    		userComments: rowEntity.userComments
                              	        	    }
                              	        	    $http({url : "bankTransaction/editTransactionFromUiGrid",
                              						headers : {
                              									'Content-Type' : 'application/json',
                              									'Accept' : 'application/json'
                              								},
                              						method : "POST",
                              						data : transactionDto
                              					}).success(function(returnData) {
                              						if (returnData.status == "success") {
                              						}else{
                              						}
                              					});
                              	        	  });
                              	        	};
                              	        	
                              	        	//search call
                              	        	$scope.callToSearch = function(){
                              	        		if(new Date($scope.fromDate) <= new Date($scope.toDate)){
                              						var fmDate = new Date($scope.fromDate);
                              						var dd = String(fmDate.getDate()).padStart(2, '0');
                              						var mm = String(fmDate.getMonth() + 1).padStart(2, '0');
                              						fmDate = dd + '-' + mm + '-' + fmDate.getFullYear();
                              						
                              						var tDate = new Date($scope.toDate);
                              						var dd = String(tDate.getDate()).padStart(2, '0');
                              						var mm = String(tDate.getMonth() + 1).padStart(2, '0');
                              						tDate = dd + '-' + mm + '-' + tDate.getFullYear();	
                              						
                              						var searchObject = {
                              								searchWord: $scope.searchWord,
                              								fromDate: fmDate,
                              		        	    		toDate: tDate
                              		        	    }
                              		        	    $http({url : "bankTransaction/searchPostCall",
                              							headers : {
                              										'Content-Type' : 'application/json',
                              										'Accept' : 'application/json'
                              									},
                              							method : "POST",
                              							data : searchObject
                              						}).success(function(returnData) {
                              							if (returnData.status == "success") {
                              								alert("Saved Transaction Successfully");
                              							}else{
                              								alert("Transaction Failed");
                              							}
                              						});
                              						
                              	        		}else{
                              	        			if($scope.search == null || $scope.search == "" || $scope.search == undefined || $scope.search == NaN){
                              	        			     var request = {
                              	        		              method: 'get',
                              	        		              url: 'http://casoftware-env.eba-fp44swnf.ap-south-1.elasticbeanstalk.com/bankTransaction/getAllBankTransactionsForTableView',
                              	        		              dataType: 'json',
                              	        		              contentType: "application/json"
                              	        		          };
                              	          		          $http(request)
                              	        		             .success(function (jsonData) {
                              	        		                 $scope.gridOptions.data = jsonData.result; 
                              	        		          });
                              	        			}else{
                              	        				$http.get('bankTransaction/searchGetCall/'+$scope.search).success(
                                  	    						function(response, status, headers) {
                                  	    							if (response.status == 'success') {
                                  	    								$scope.gridOptions.data = response.result;
                                  	    							} else {
                                  	    							    $scope.gridOptions.data = [];
                                  	    							}
                                  	    						});
                              	        			}
                              	        		}
                              	        		
                              	        	};
                              	        	//end of search call
                              		} ]);

uiSelectWrap.$inject = [ '$document', 'uiGridEditConstants' ];
function uiSelectWrap($document, uiGridEditConstants) {
	return function link($scope, $elm, $attr) {
		$document.on('click', docClick);
		function docClick(evt) {
			if ($(evt.target).closest('.ui-select-container').size() === 0) {
				$scope.$emit(uiGridEditConstants.events.END_CELL_EDIT);
				$document.off('click', docClick);
			}
		}
	};
}