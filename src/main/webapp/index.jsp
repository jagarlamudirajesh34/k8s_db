<!DOCTYPE html>
<html>
<head>

<title>SamvitApp</title>

<meta charset="UTF-8">

<script type="text/javascript">
	var contextPath = "${pageContext.request.contextPath}";
</script>

<link rel="stylesheet" href="css/flatpickr.min.css" />
<script type="text/javascript" src="js/lib/jquery.min.js"></script>
<!-- <script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script> -->

<script type="text/javascript" src="js/lib/bootstrap.min.js"></script>

<!--   <script type="text/javascript" src="js/lib/jquery.ui.tabs.min.js"></script> -->
<script type="text/javascript" src="js/lib/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="js/lib/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="js/lib/jquery.ui.timepicker.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-animate.js"></script>
<script
	src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-sanitize.js"></script>
<script
	src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-2.1.4.js"></script>

<script type="text/javascript" src="js/lib/angular.min.js"></script>
<script type="text/javascript" src="js/lib/angular-route.min.js"></script>
<script type="text/javascript" src="js/lib/angular-material.min.js"></script>
<script type="text/javascript" src="js/lib/angular-cookies.js"></script>
<script type="text/javascript" src="js/lib/query-string.js"></script>
<script type="text/javascript" src="js/lib/angular-oauth2.js"></script>
<script type="text/javascript" src="js/lib/http-auth-interceptor.js"></script>
<script type="text/javascript" src="js/lib/angular-ui-router.js"></script>
<script type="text/javascript" src="js/lib/b64.js"></script>
<script type="text/javascript" src="js/lib/ng-file-upload.min.js"></script>

<script type="text/javascript"
	src="js/lib/ui-bootstrap-tpls-0.12.1.js"></script>
<script type="text/javascript" src="js/lib/flatpickr.min.js"></script>
<script type="text/javascript" src="js/lib/bootstrap-datepicker.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.6.3/ui-grid.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-grid/4.6.3/ui-grid.min.css" type="text/css">
    
<script type="text/javascript" src="js/samvitApp.js"></script>
<script type="text/javascript" src="js/controllers/homeController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminUsersController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminAllBankTransactionsController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminBankCashbookController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminGroupsLedgerController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminAllUsersCashbookController.js"></script>
<script type="text/javascript" src="js/controllers/admin/testController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminLedgerMasterController.js"></script>

<script src="http://ui-grid.info/docs/grunt-scripts/csv.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/pdfmake.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/vfs_fonts.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/lodash.min.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/jszip.min.js"></script>
    <script src="http://ui-grid.info/docs/grunt-scripts/excel-builder.dist.js"></script>
    <script src="http://ui-grid.info/release/ui-grid.js"></script>
    <link rel="stylesheet" href="http://ui-grid.info/release/ui-grid.css" type="text/css">
    
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-sanitize.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.8/select.js"></script>
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.5/select2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-select/0.19.8/select.css">

<link data-require="bootstrap@*" data-semver="3.3.2" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" />
    <script data-require="jquery@2.1.3" data-semver="2.1.3" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.8.5/css/selectize.default.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/angular-ui/ui-grid.info/v3.0.7/release/3.0.7/ui-grid.min.css" type="text/css" />
    
    
<script type="text/javascript"
	src="js/controllers/user/userController.js"></script>
<script type="text/javascript" src="js/controllers/headerController.js"></script>

<!-- Admin -->
<script type="text/javascript"
	src="js/controllers/admin/adminHomeController.js"></script>
<script type="text/javascript" src="js/controllers/admin/adminUsersController.js"></script>

<!-- <script type="text/javascript" src="js/controllers/transport/transportHeaderController.js"></script> -->

<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css" />
<link rel="stylesheet" href="css/jquery.ui.timepicker.css" />
<link rel="stylesheet" href="css/jquery-ui-1.10.0.custom.min.css" />

<link rel="stylesheet" href="css/samvit.css" />
<link rel="stylesheet" href="css/manager.css" />
<link rel="stylesheet" href="css/map.css" />

    

</head>

<body data-ng-app="samvit">
	<div id="mainTemplate" data-ng-controller="homeController"
		data-ng-init="initPage()">
		<div data-ng-view class="mid"></div>
	</div>
</body>
</html>