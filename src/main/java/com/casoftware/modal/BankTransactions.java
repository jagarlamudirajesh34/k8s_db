package com.casoftware.modal;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bank_transaction")
public class BankTransactions {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "transaction_date", nullable = false)
	private Date transactionDate;

	@Column(name = "description", nullable = false)
	private String description;

	@Column(name = "cheque_no", nullable = false)
	private String chequeNo;

	@Column(name = "debit", nullable = false)
	private int debit;

	@Column(name = "credit", nullable = false)
	private int credit;

	@Column(name = "balance", nullable = false)
	private int balance;

	@Column(name = "category", nullable = false)
	private String category;

	@Column(name = "remarks", nullable = false)
	private String remarks;

	@ManyToOne
	@JoinColumn(name = "bank_id")
	private Bank bank;

	@Column(name = "created_date", nullable = false)
	private Date createdDate;

	@Column(name = "updated_date")
	private Date updatedDate;

	@Column(name = "deleted_yn", nullable = false)
	private Boolean deletedYn;

	// 0 - pending, 1 - approved, 2 - rejected
	@Column(name = "approved_pending", nullable = false)
	private int approvedPending;

	@Column(name = "user_comments")
	private String userComments;

	@Column(name = "admin_comments")
	private String adminComments;

	@Column(name = "party")
	private String party;

	@Column(name = "bill_path")
	private String billPath;

	@Column(name = "ledger_id", nullable = true)
	private int ledgerId;

	@Column(name = "ledger_name", nullable = true)
	private String ledgerName;

	@Column(name = "credit_ledger_id")
	private int creditLedgerId;

	@Column(name = "credit_ledger_name", nullable = true)
	private String creditLedgerName;

	public int getApprovedPending() {
		return approvedPending;
	}

	public void setApprovedPending(int approvedPending) {
		this.approvedPending = approvedPending;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getChequeNo() {
		return chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public int getDebit() {
		return debit;
	}

	public void setDebit(int debit) {
		this.debit = debit;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(Bank bank) {
		this.bank = bank;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getDeletedYn() {
		return deletedYn;
	}

	public void setDeletedYn(Boolean deletedYn) {
		this.deletedYn = deletedYn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getUserComments() {
		return userComments;
	}

	public void setUserComments(String userComments) {
		this.userComments = userComments;
	}

	public String getAdminComments() {
		return adminComments;
	}

	public void setAdminComments(String adminComments) {
		this.adminComments = adminComments;
	}

	public String getParty() {
		return party;
	}

	public void setParty(String party) {
		this.party = party;
	}

	public String getBillPath() {
		return billPath;
	}

	public void setBillPath(String billPath) {
		this.billPath = billPath;
	}

	public int getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(int ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getLedgerName() {
		return ledgerName;
	}

	public void setLedgerName(String ledgerName) {
		this.ledgerName = ledgerName;
	}

	public int getCreditLedgerId() {
		return creditLedgerId;
	}

	public void setCreditLedgerId(int creditLedgerId) {
		this.creditLedgerId = creditLedgerId;
	}

	public String getCreditLedgerName() {
		return creditLedgerName;
	}

	public void setCreditLedgerName(String creditLedgerName) {
		this.creditLedgerName = creditLedgerName;
	}

	@Override
	public String toString() {
		return "BankTransactions [id=" + id + ", transactionDate=" + transactionDate + ", description=" + description
				+ ", chequeNo=" + chequeNo + ", debit=" + debit + ", credit=" + credit + ", balance=" + balance
				+ ", category=" + category + ", remarks=" + remarks + ", bank=" + bank + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + ", deletedYn=" + deletedYn + ", approvedPending=" + approvedPending
				+ ", userComments=" + userComments + ", adminComments=" + adminComments + ", party=" + party
				+ ", billPath=" + billPath + ", ledgerId=" + ledgerId + ", ledgerName=" + ledgerName
				+ ", creditLedgerId=" + creditLedgerId + ", creditLedgerName=" + creditLedgerName + "]";
	}

}
