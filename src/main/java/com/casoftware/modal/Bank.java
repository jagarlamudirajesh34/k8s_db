package com.casoftware.modal;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bank")
public class Bank implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "bank_name", nullable = false)
	private String bankName;
	
	@Column(name = "type", nullable = false)
	private String type;
	
	@Column(name = "institution", nullable = false)
	private String institution;
	
	@Column(name = "last_updated", nullable = false)
	private Date lastUpdated;
	
	@Column(name = "balance", nullable = false)
	private Double balance;
	
	@Column(name = "locked_amount", nullable = false)
	private Double lockedAmount;
	
	@Column(name = "over_draft", nullable = false)
	private Double overDraft;
	
	@Column(name = "bank_cashbook", nullable = false)
	private Boolean bankCashbook;


	@Override
	public String toString() {
		return "Bank [id=" + id + ", bankName=" + bankName + ", type=" + type + ", institution=" + institution
				+ ", lastUpdated=" + lastUpdated + ", balance=" + balance + ", lockedAmount=" + lockedAmount
				+ ", overDraft=" + overDraft + ", bankCashbook=" + bankCashbook + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getLockedAmount() {
		return lockedAmount;
	}

	public void setLockedAmount(Double lockedAmount) {
		this.lockedAmount = lockedAmount;
	}

	public Double getOverDraft() {
		return overDraft;
	}

	public void setOverDraft(Double overDraft) {
		this.overDraft = overDraft;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getBankCashbook() {
		return bankCashbook;
	}

	public void setBankCashbook(Boolean bankCashbook) {
		this.bankCashbook = bankCashbook;
	}
}