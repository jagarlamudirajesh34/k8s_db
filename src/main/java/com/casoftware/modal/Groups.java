package com.casoftware.modal;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "groups_ledgers")
public class Groups {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Column(name = "group_name", nullable = false)
	private String groupName;

	@Column(name = "group_ledger", nullable = false)
	private Boolean groupLedger;

	@Column(name = "cash_ledger", nullable = false)
	private Boolean cashLedger;

	@Column(name = "deleted_yn", nullable = false)
	private Boolean deletedYn;

	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "parent_group_id")
	private Groups group;
	
	@ManyToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "ledger_master_data_id")
	private LedgerMasterData ledgerMasterData;
	
	@Column(name = "main_group")
	private Boolean mainGroup;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Boolean getGroupLedger() {
		return groupLedger;
	}

	public void setGroupLedger(Boolean groupLedger) {
		this.groupLedger = groupLedger;
	}

	public Boolean getCashLedger() {
		return cashLedger;
	}

	public void setCashLedger(Boolean cashLedger) {
		this.cashLedger = cashLedger;
	}

	public Boolean getDeletedYn() {
		return deletedYn;
	}

	public void setDeletedYn(Boolean deletedYn) {
		this.deletedYn = deletedYn;
	}

	public Groups getGroup() {
		return group;
	}

	public void setGroup(Groups group) {
		this.group = group;
	}

	public Boolean getMainGroup() {
		return mainGroup;
	}

	public void setMainGroup(Boolean mainGroup) {
		this.mainGroup = mainGroup;
	}

	public LedgerMasterData getLedgerMasterData() {
		return ledgerMasterData;
	}

	public void setLedgerMasterData(LedgerMasterData ledgerMasterData) {
		this.ledgerMasterData = ledgerMasterData;
	}

	@Override
	public String toString() {
		return "Groups [id=" + id + ", groupName=" + groupName + ", groupLedger=" + groupLedger + ", cashLedger="
				+ cashLedger + ", deletedYn=" + deletedYn + ", group=" + group + ", ledgerMasterData="
				+ ledgerMasterData + ", mainGroup=" + mainGroup + "]";
	}

}
