package com.casoftware.rest.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.casoftware.dto.TransactionDto;
import com.casoftware.modal.BankTransactions;
import com.casoftware.service.BankTransactionsService;
import com.casoftware.spring.config.CaSoftwareConfig;
import com.casoftware.util.ServiceStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/bankTransaction")
public class BankTransactionsController {

	@Autowired
	BankTransactionsService bankTransactionsService;

	// for getting all users details
	@RequestMapping(value = "/getAllBankTransactions", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllBankTransactions() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<BankTransactions> allBanksList = bankTransactionsService.getAllBankTransactions();
			if (!allBanksList.isEmpty() && allBanksList != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allBanksList);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("no bank transactions found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	@RequestMapping(value = "/addTransaction", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = { "multipart/form-data" })
	public @ResponseBody ServiceStatus addUser(@Valid @RequestParam("bankTransaction") String bankTransactionJSONString,
			@RequestParam(value = "paymentSlipPic", required = false) CommonsMultipartFile paymentSlipPic,
			@RequestParam("transactionDate") String transactionDate) throws IOException {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			BankTransactions bankTransaction = new ObjectMapper().readValue(bankTransactionJSONString,
					BankTransactions.class);
			Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(transactionDate);
			if (bankTransaction.getCredit() == bankTransaction.getDebit() && bankTransaction.getCredit() == 0) {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Both credit and debit should not be 0");
			} else {
				bankTransaction.setTransactionDate(date1);
				bankTransaction.setApprovedPending(0);
				bankTransaction.setCreatedDate(Calendar.getInstance().getTime());
				bankTransaction.setUpdatedDate(Calendar.getInstance().getTime());
				bankTransaction.setDeletedYn(false);
				if (bankTransaction.getUserComments() != null) {
					bankTransaction.setDescription(bankTransaction.getUserComments());
				}
				bankTransaction.setId(1L);
				// bankTransactionsService.saveBankTransaction(bankTransaction);
				if (paymentSlipPic != null) {
					String profilePicName = bankTransaction.getId().toString();
					String fileExtension = paymentSlipPic.getOriginalFilename();
					profilePicName = profilePicName + Calendar.getInstance().getTimeInMillis()
							+ fileExtension.substring(fileExtension.lastIndexOf('.'));
					String awsAccessKeyId = CaSoftwareConfig.getInstance().getConfigValue("AWSAccessKeyId");
					String awsSecretKey = CaSoftwareConfig.getInstance().getConfigValue("AWSSecretKey");
					String bucketName = CaSoftwareConfig.getInstance().getConfigValue("billPictureBucket");
					try {
						AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
						AmazonS3 s3client = new AmazonS3Client(credentials);
						byte[] imageInByte = paymentSlipPic.getBytes();
						ObjectMetadata meta = new ObjectMetadata();
						meta.setContentLength(imageInByte.length);
						InputStream stream1 = new ByteArrayInputStream(imageInByte);
						meta.setContentType(paymentSlipPic.getContentType());
						s3client.putObject(bucketName, profilePicName, stream1, meta);
					} catch (Exception e) {
						e.printStackTrace();
					}
					// String completeFilePath = bucketName + "/" +
					// profilePicName;
					bankTransaction.setBillPath(profilePicName);
					serviceStatus.setStatus("success");
					serviceStatus.setMessage(profilePicName);
					serviceStatus.setResult(bankTransaction.getId());
				} else {
					serviceStatus.setStatus("success");
					serviceStatus.setMessage("NA");
					serviceStatus.setResult(bankTransaction.getId());
					bankTransaction.setBillPath("NA");
				}
				// bankTransactionsService.editBankTransaction(bankTransaction);
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("exception occured while adding / editing bank transaction");
			serviceStatus.setResult(e.getLocalizedMessage());
		}
		return serviceStatus;
	}

	@RequestMapping(value = "/downloadBill/{billPath}/{ext}", method = RequestMethod.GET)
	public void downloadPriviewPic(HttpServletResponse response, @PathVariable("billPath") String billPath,
			@PathVariable("ext") String ext) {
		try {
			if (billPath != null) {
				OutputStream out = response.getOutputStream();
				response.addHeader("Content-Disposition", "attachment; filename=" + billPath + "." + ext);
				// response.setContentType("application/pdf");
				String bucketName = CaSoftwareConfig.getInstance().getConfigValue("billPictureBucket");
				String awsAccessKeyId = CaSoftwareConfig.getInstance().getConfigValue("AWSAccessKeyId");
				String awsSecretKey = CaSoftwareConfig.getInstance().getConfigValue("AWSSecretKey");

				AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
				AmazonS3 s3client = new AmazonS3Client(credentials);
				GetObjectRequest requestAWS = new GetObjectRequest(bucketName, billPath + "." + ext);
				S3Object object = s3client.getObject(requestAWS);
				InputStream stream = object.getObjectContent();
				byte[] buffer = new byte[100024];
				for (int length = 0; (length = stream.read(buffer)) > 0;) {
					out.write(buffer, 0, length);
				}
			}
		} catch (Exception e) {
		}
	}

	// for getting all bank transactions for edit table view
	@RequestMapping(value = "/getAllBankTransactionsForTableView", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllBankTransactionsForTableView() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<BankTransactions> allTrans = bankTransactionsService.getAllBankTransactionsForTableView();
			List<TransactionDto> allTransacDto = new ArrayList<TransactionDto>();
			if (allTrans != null & !allTrans.isEmpty()) {
				for (BankTransactions bt : allTrans) {
					TransactionDto td = new TransactionDto();
					td.setTransactionId(bt.getId());
					td.setBankName(bt.getBank().getBankName());
					td.setCheque(bt.getChequeNo());
					td.setDescription(bt.getDescription());
					td.setBalance(bt.getBalance());
					td.setDebit(bt.getDebit());
					td.setCredit(bt.getCredit());
					td.setTransactionDate(bt.getTransactionDate());
					td.setUserComments(bt.getUserComments());
					td.setCreditLedgerId(bt.getCreditLedgerId());
					td.setCreditLedgerName(bt.getCreditLedgerName());
					td.setDebitLedgerId(bt.getLedgerId());
					td.setDebitLedgerName(bt.getLedgerName());
					allTransacDto.add(td);
				}
				serviceStatus.setResult(allTransacDto);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Retrived all transactions successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("no transactions found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all bank transactions for edit table view based on from and
	// to dates
	@RequestMapping(value = "/getAllBankTransactionsForTableView/{fromDate}/{toDate}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllBankTransactionsForTableView(@PathVariable("fromDate") String fromDate,
			@PathVariable("toDate") String toDate) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			Date sDate = new SimpleDateFormat("dd-MM-yyyy").parse(fromDate);
			Date eDate = new SimpleDateFormat("dd-MM-yyyy").parse(toDate);
			List<BankTransactions> allTrans = bankTransactionsService.getAllBankTransactionsForTableView(sDate, eDate);
			List<TransactionDto> allTransacDto = new ArrayList<TransactionDto>();
			if (allTrans != null & !allTrans.isEmpty()) {
				for (BankTransactions bt : allTrans) {
					TransactionDto td = new TransactionDto();
					td.setTransactionId(bt.getId());
					td.setBankName(bt.getBank().getBankName());
					td.setCheque(bt.getChequeNo());
					td.setDescription(bt.getDescription());
					td.setBalance(bt.getBalance());
					td.setDebit(bt.getDebit());
					td.setCredit(bt.getCredit());
					td.setTransactionDate(bt.getTransactionDate());
					td.setUserComments(bt.getUserComments());
					td.setCreditLedgerId(bt.getCreditLedgerId());
					td.setCreditLedgerName(bt.getCreditLedgerName());
					td.setDebitLedgerId(bt.getLedgerId());
					td.setDebitLedgerName(bt.getLedgerName());
					allTransacDto.add(td);
				}
				serviceStatus.setResult(allTransacDto);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Retrived all transactions successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("no transactions found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// edit Transaction from UI Grid
	@RequestMapping(value = "/editTransactionFromUiGrid", method = RequestMethod.POST, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus editTransactionFromUiGrid(@RequestBody TransactionDto transactionDto) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (transactionDto.getTransactionId() != 0) {
				bankTransactionsService.editTransactionFromUiGrid(transactionDto);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Transaction saved successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid data");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// update ledgers info in bank transaction
	@RequestMapping(value = "/updateLedgersInTransactioninUiGrid", method = RequestMethod.POST, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus updateLedgersInTransactioninUiGrid(@RequestBody TransactionDto transactionDto) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (transactionDto.getTransactionId() != 0) {
				bankTransactionsService.updateLedgersInTransactioninUiGrid(transactionDto);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Saved succeesfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid data");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// search get call on ui grid table
	@RequestMapping(value = "/searchGetCall/{searchWord}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus searchGetCall(@PathVariable("searchWord") String searchWord) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (searchWord != null && !searchWord.isEmpty()) {
				List<BankTransactions> allTrans = bankTransactionsService.getTransactionsBasedOnSearch(searchWord);
				List<TransactionDto> allTransacDto = new ArrayList<TransactionDto>();
				if (allTrans != null && !allTrans.isEmpty()) {
					for (BankTransactions bt : allTrans) {
						TransactionDto td = new TransactionDto();
						td.setTransactionId(bt.getId());
						td.setBankName(bt.getBank().getBankName());
						td.setCheque(bt.getChequeNo());
						td.setDescription(bt.getDescription());
						td.setBalance(bt.getBalance());
						td.setDebit(bt.getDebit());
						td.setCredit(bt.getCredit());
						td.setTransactionDate(bt.getTransactionDate());
						td.setUserComments(bt.getUserComments());
						td.setCreditLedgerId(bt.getCreditLedgerId());
						td.setCreditLedgerName(bt.getCreditLedgerName());
						td.setDebitLedgerId(bt.getLedgerId());
						td.setDebitLedgerName(bt.getLedgerName());
						allTransacDto.add(td);
					}
					serviceStatus.setStatus("success");
					serviceStatus.setResult(allTransacDto);
					serviceStatus.setMessage("No transactions is available for above search criteria");
				} else {
					serviceStatus.setStatus("failure");
					serviceStatus.setMessage("No transactions is available for above search criteria");
				}
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid data");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

}
