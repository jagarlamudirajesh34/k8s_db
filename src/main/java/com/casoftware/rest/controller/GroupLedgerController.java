package com.casoftware.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.casoftware.dto.LedgerDto;
import com.casoftware.modal.Groups;
import com.casoftware.service.GroupLedgerService;
import com.casoftware.util.ServiceStatus;

@RestController
@RequestMapping("/groupLedger")
public class GroupLedgerController {

	@Autowired
	GroupLedgerService groupLedgerService;

	// for getting all ledger with group details
	@RequestMapping(value = "/getAllList", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllList() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<LedgerDto> allList = groupLedgerService.getAllList();
			if (allList != null && !allList.isEmpty()) {
				serviceStatus.setResult(allList);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("retrived list successfull");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("no list found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all ledger with group details
	@RequestMapping(value = "/getAllActiveGroups", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveGroups() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> allActiveGroups = groupLedgerService.getAllActiveGroups();
			if (allActiveGroups != null && !allActiveGroups.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allActiveGroups);
				serviceStatus.setMessage("Retrived all active group list");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active groups found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// ============================ New ================
	// for getting all ledger with group details
	@RequestMapping(value = "/getAllActiveTopGroups", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveTopGroups() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> allActiveGroups = groupLedgerService.getAllActiveTopGroups();
			if (allActiveGroups != null && !allActiveGroups.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allActiveGroups);
				serviceStatus.setMessage("Retrived all active group list");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active groups found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all liability main groups list
	@RequestMapping(value = "/getAllLiabilityMainGroups", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllLiabilityMainGroups() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> getAllLiabilityMainGroups = groupLedgerService.getAllLiabilityMainGroups();
			if (getAllLiabilityMainGroups != null && !getAllLiabilityMainGroups.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(getAllLiabilityMainGroups);
				serviceStatus.setMessage("Retrived all getAllLiabilityMainGroups list");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active groups found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all liability main groups list
	@RequestMapping(value = "/getSubGroups/{groupId}", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getSubGroups(@PathVariable("groupId") Long groupId) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> getSubGroups = groupLedgerService.getSubGroups(groupId);
			if (getSubGroups != null && !getSubGroups.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(getSubGroups);
				serviceStatus.setMessage("Retrived all get sub groups list");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active sub groups found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all ledgers
	@RequestMapping(value = "/getAllActiveLedgers", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveLedgers() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> getAllActiveLedgers = groupLedgerService.getAllActiveLedgers();
			if (getAllActiveLedgers != null && !getAllActiveLedgers.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(getAllActiveLedgers);
				serviceStatus.setMessage("Retrived all active ledgers list");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active ledgers found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// edit ledger master data
	// edit Transaction from UI Grid
	@RequestMapping(value = "/editLedgerMasterData", method = RequestMethod.POST, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus editTransactionFromUiGrid(@RequestBody Groups groups) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (groups.getId() != 0 && groups.getLedgerMasterData() != null) {
				groupLedgerService.editLedgerMasterData(groups);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Ledger Master Data saved successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid data");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all ledgers
	@RequestMapping(value = "/getAllActiveLedgersForLedgerMaster", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveLedgersForLedgerMaster() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> getAllActiveLedgers = groupLedgerService.getAllActiveLedgers();
			if (getAllActiveLedgers != null && !getAllActiveLedgers.isEmpty()) {
				List<LedgerDto> ledgerList = new ArrayList<LedgerDto>();
				for (Groups grp : getAllActiveLedgers) {
					LedgerDto ld = new LedgerDto();
					ld.setId(grp.getId());
					ld.setGroupName(grp.getGroupName());
					ld.setGroupHierarchyName(String.join(" << ", checkSubGroup(grp.getGroup())));
					if (grp.getLedgerMasterData() != null) {
						ld.setLedgerMasterData(grp.getLedgerMasterData());
						String data = grp.getLedgerMasterData().getName();
						if (grp.getLedgerMasterData().getAddress() != null) {
							data = data + ", " + grp.getLedgerMasterData().getAddress();
						}
						if (grp.getLedgerMasterData().getAadhar() != null) {
							data = data + ", " + grp.getLedgerMasterData().getAadhar();
						}
						if (grp.getLedgerMasterData().getState() != null) {
							data = data + ", " + grp.getLedgerMasterData().getState();
						}
						if (grp.getLedgerMasterData().getPincode() != null) {
							data = data + ", " + grp.getLedgerMasterData().getPincode();
						}
						if (grp.getLedgerMasterData().getEmail() != null) {
							data = data + ", " + grp.getLedgerMasterData().getEmail();
						}
						if (grp.getLedgerMasterData().getPhone() != null) {
							data = data + ", " + grp.getLedgerMasterData().getPhone();
						}
						if (grp.getLedgerMasterData().getPhone() != null) {
							data = data + ", " + grp.getLedgerMasterData().getAadhar();
						}
						if (grp.getLedgerMasterData().getPan() != null) {
							data = data + ", " + grp.getLedgerMasterData().getPan();
						}
						if (grp.getLedgerMasterData().getGstNumber() != null) {
							data = data + ", " + grp.getLedgerMasterData().getGstNumber();
						}
						ld.setLedgerMasterInfo(data);
					}
					ledgerList.add(ld);
				}
				serviceStatus.setStatus("success");
				serviceStatus.setResult(ledgerList);
				serviceStatus.setMessage("Retrived all active ledgers list.");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No active ledgers found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all ledgers
	@RequestMapping(value = "/searchOnLedgerMasterData/{searchWord}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus searchOnLedgerMasterData(@PathVariable("searchWord") String searchWord) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (searchWord != null) {
				List<Groups> groupsList = groupLedgerService.searchOnLedgerMasterData(searchWord);
				if (groupsList != null && !groupsList.isEmpty()) {
					List<LedgerDto> ledgerList = new ArrayList<LedgerDto>();
					for (Groups grp : groupsList) {
						LedgerDto ld = new LedgerDto();
						ld.setId(grp.getId());
						ld.setGroupName(grp.getGroupName());
						ld.setGroupHierarchyName(String.join(" << ", checkSubGroup(grp.getGroup())));
						if (grp.getLedgerMasterData() != null) {
							ld.setLedgerMasterData(grp.getLedgerMasterData());
							String data = grp.getLedgerMasterData().getName();
							if (grp.getLedgerMasterData().getAddress() != null) {
								data = data + ", " + grp.getLedgerMasterData().getAddress();
							}
							if (grp.getLedgerMasterData().getAadhar() != null) {
								data = data + ", " + grp.getLedgerMasterData().getAadhar();
							}
							if (grp.getLedgerMasterData().getState() != null) {
								data = data + ", " + grp.getLedgerMasterData().getState();
							}
							if (grp.getLedgerMasterData().getPincode() != null) {
								data = data + ", " + grp.getLedgerMasterData().getPincode();
							}
							if (grp.getLedgerMasterData().getEmail() != null) {
								data = data + ", " + grp.getLedgerMasterData().getEmail();
							}
							if (grp.getLedgerMasterData().getPhone() != null) {
								data = data + ", " + grp.getLedgerMasterData().getPhone();
							}
							if (grp.getLedgerMasterData().getPhone() != null) {
								data = data + ", " + grp.getLedgerMasterData().getAadhar();
							}
							if (grp.getLedgerMasterData().getPan() != null) {
								data = data + ", " + grp.getLedgerMasterData().getPan();
							}
							if (grp.getLedgerMasterData().getGstNumber() != null) {
								data = data + ", " + grp.getLedgerMasterData().getGstNumber();
							}
							ld.setLedgerMasterInfo(data);
						}
						ledgerList.add(ld);
					}
					serviceStatus.setStatus("success");
					serviceStatus.setResult(ledgerList);
					serviceStatus.setMessage("Ledgers list retrived successfully");
				} else {
					serviceStatus.setStatus("failure");
					serviceStatus.setMessage("No Ledgers found for search criteria");
				}
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid Data");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	public List<String> checkSubGroup(Groups group) {
		Groups groups = group;
		List<String> array = new ArrayList<String>();
		while (groups != null) {
			array.add(groups.getGroupName().trim());
			groups = groups.getGroup();
		}
		return array;
	}

}
