package com.casoftware.rest.controller;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.User;
import com.casoftware.modal.UserCashbook;
import com.casoftware.service.MainService;
import com.casoftware.spring.config.CaSoftwareConfig;
import com.casoftware.util.ServiceStatus;

@RestController
@RequestMapping("/main")
public class MainController {

	@Autowired
	MainService mainService;

	// for getting all users details
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllBankDetails() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<User> allUsers = mainService.getAllUsers();
			if (allUsers != null && !allUsers.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allUsers);
				serviceStatus.setMessage("retrived all users successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No Users found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all users and cashbook infoetails
	@RequestMapping(value = "/getAllActiveUsersCashbook", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveUsersCashbook() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<UserCashbook> allUsersCashbook = mainService.getAllActiveUsersCashbook();
			if (allUsersCashbook != null && !allUsersCashbook.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allUsersCashbook);
				serviceStatus.setMessage("retrived all users successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No Users found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting all cash book transactions based on cashbook id
	@RequestMapping(value = "/getAllCashbookTransactionsBasedOnCashbookId/{cashbookId}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllCashbookTransactionsBasedOnCashbookId(@PathVariable("cashbookId") Long cashbookId) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<BankTransactions> allCashbookTransactions = mainService
					.getAllCashbookTransactionsBasedOnCashbookId(cashbookId);
			for (BankTransactions bt : allCashbookTransactions) {
				bt.setCreatedDate(null);
				bt.setUpdatedDate(null);
				bt.setCategory(null);
				bt.setRemarks(null);
				bt.setDeletedYn(null);
			}
			if (allCashbookTransactions != null && !allCashbookTransactions.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allCashbookTransactions);
				serviceStatus.setMessage("Retrived all Transactions successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No Transactions found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// for getting particular cash book transactions based on cashbook id, date,
	// status
	@RequestMapping(value = "/getAllCashbookTransactionsBasedOnCashbookIdDateStatus/{cashbookId}/{startDate}/{endDate}/{status}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllCashbookTransactionsBasedOnCashbookIdDateStatus(
			@PathVariable("cashbookId") Long cashbookId, @PathVariable("startDate") String startDate,
			@PathVariable("endDate") String endDate, @PathVariable("status") String status) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {

			Date sDate = new SimpleDateFormat("dd-MM-yyyy").parse(startDate);
			Date eDate = new SimpleDateFormat("dd-MM-yyyy").parse(endDate);
			List<BankTransactions> allCashbookTransactions = mainService
					.getAllCashbookTransactionsBasedOnCashbookIdDateStatus(cashbookId, sDate, eDate, status);
			for (BankTransactions bt : allCashbookTransactions) {
				bt.setCreatedDate(null);
				bt.setUpdatedDate(null);
				bt.setCategory(null);
				bt.setRemarks(null);
				bt.setDeletedYn(null);
			}
			if (allCashbookTransactions != null && !allCashbookTransactions.isEmpty()) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allCashbookTransactions);
				serviceStatus.setMessage("Retrived All Transactions Successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("No Transactions found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// approve or reject transaction
	@RequestMapping(value = "/approveRejectTransaction", method = RequestMethod.POST, produces = {
			"application/json" }, consumes = { "application/json" })
	@ResponseBody
	public ServiceStatus approveRejectTransaction(@RequestBody BankTransactions bankTransaction) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			if (bankTransaction.getId() != 0) {
				mainService.approveRejectTransaction(bankTransaction);
				serviceStatus.setStatus("success");
				serviceStatus.setMessage("Approve or Reject transaction successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Invalid input");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("Exception occured");
		}
		return serviceStatus;
	}

	// view bill by admin user
	@RequestMapping(value = "/viewBillByAdmin/{file}", method = RequestMethod.GET)
	public void downloadFile(HttpServletResponse response, @PathVariable("file") String file) {
		try {
			OutputStream out = response.getOutputStream();
			response.addHeader("Content-Disposition", "attachment; filename=" + file);
			response.setContentType("image/jpeg");
			String awsAccessKeyId = CaSoftwareConfig.getInstance().getConfigValue("AWSAccessKeyId");
			String awsSecretKey = CaSoftwareConfig.getInstance().getConfigValue("AWSSecretKey");
			AWSCredentials credentials = new BasicAWSCredentials(awsAccessKeyId, awsSecretKey);
			AmazonS3 s3client = new AmazonS3Client(credentials);
		} catch (Exception e) {

		}
	}

}
