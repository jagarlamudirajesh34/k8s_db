package com.casoftware.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.casoftware.modal.Bank;
import com.casoftware.modal.Groups;
import com.casoftware.modal.UserCashbook;
import com.casoftware.service.BankService;
import com.casoftware.util.ServiceStatus;

@RestController
@RequestMapping("/bank")
public class BankController {

	@Autowired
	BankService bankService;

	// for getting all banks based on bank or cashbook details
	@RequestMapping(value = "/getAllBanks/{bankCashbook}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllBanks(@PathVariable("bankCashbook") Boolean bankCashbook) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Bank> allBanks = bankService.getAllBanks(bankCashbook);
			if (!allBanks.isEmpty() && allBanks != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allBanks);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	// for getting all banks and cash books
	@RequestMapping(value = "/getAllBanksCashbooks", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllBanksCashbooks() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Bank> allBanks = bankService.getAllBanksCashbooks();
			if (!allBanks.isEmpty() && allBanks != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allBanks);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	// for getting all banks and cash books
	@RequestMapping(value = "/getCashbookBasedOnUser/{userId}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getCashbookBasedOnUser(@PathVariable("userId") Long userId) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			UserCashbook userCashBook = bankService.getCashbookBasedOnUser(userId);
			if (userCashBook != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(userCashBook.getBank());
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	// for getting all banks and cash books
	@RequestMapping(value = "/getAllActiveCashbookLedgers", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveCashbookLedgers() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> allActiveLedgers = bankService.getAllActiveCashbookLedgers();
			if (allActiveLedgers != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allActiveLedgers);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	// for getting all banks and cash books
	@RequestMapping(value = "/getAllActiveBankLedgers", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllActiveBankLedgers() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Groups> allActiveLedgers = bankService.getAllActiveBankLedgers();
			if (allActiveLedgers != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(allActiveLedgers);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

	// for getting all banks and cash books
	@RequestMapping(value = "/getBankOrCashbookBasedOnId/{id}", method = RequestMethod.GET, produces = {
			"application/json" })
	@ResponseBody
	public ServiceStatus getBankOrCashbookBasedOnId(@PathVariable("id") Long id) {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			Bank bank = bankService.getBankOrCashbookBasedOnId(id);
			if (bank != null) {
				serviceStatus.setStatus("success");
				serviceStatus.setResult(bank);
				serviceStatus.setMessage("Retrived response successfully");
			} else {
				serviceStatus.setStatus("failure");
				serviceStatus.setMessage("Retrived response successfully");
			}
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setStatus("failure");
			serviceStatus.setMessage("Exception Occured");
		}
		return serviceStatus;
	}

}
