package com.casoftware.dao;

import java.util.List;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.Groups;
import com.casoftware.modal.UserCashbook;

public interface BankDao extends Dao {

	public Bank checkBankDetailsAvailableInDB(String bankName);

	public List<Bank> getAllBanks(Boolean bankCashbook);

	public List<Bank> getAllBanksCashbooks();

	public List<BankTransactions> getTotalBankTransactionsinDBForBank(Long bankId);

	public UserCashbook getCashbookBasedOnUser(Long userId);

	public List<Groups> getAllActiveCashbookLedgers();
	
	public List<Groups> getAllActiveBankLedgers();
	
	public Bank getBankOrCashbookBasedOnId(Long id);
	
}
