package com.casoftware.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.casoftware.dto.LedgerDto;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.Groups;

@Repository("groupLedgerDao")
public class GroupLedgerDaoImpl extends AbstractDao implements GroupLedgerDao {

	@Override
	public List<LedgerDto> getAllList() {

		// getSession().createCriteria(Groups.class).list();

		List<Groups> allGroups = getSession().createCriteria(Groups.class).add(Restrictions.eq("mainGroup", true))
				.list();
		List<LedgerDto> allLedgerDto = new ArrayList<LedgerDto>();
		for (Groups g : allGroups) {
			LedgerDto ld = new LedgerDto();
			ld.setId(g.getId());
			ld.setGroupName(g.getGroupName().trim());
			allLedgerDto.add(ld);
		}
		for (LedgerDto ld : allLedgerDto) {
			List<Groups> allGroups1 = getSession().createCriteria(Groups.class)
					.add(Restrictions.eq("group.id", ld.getId())).list();
			List<LedgerDto> allLedgerDto1 = new ArrayList<LedgerDto>();
			for (Groups g : allGroups1) {
				LedgerDto ldt = new LedgerDto();
				ldt.setId(g.getId());
				ldt.setGroupName(g.getGroupName());
				allLedgerDto1.add(ldt);
			}
			ld.setLedgerDtoList(allLedgerDto1);
		}
		return allLedgerDto;

	}

	@Override
	public List<Groups> getAllActiveGroups() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("groupLedger", true))
				.add(Restrictions.eq("deletedYn", false)).list();
	}

	@Override
	public List<Groups> getAllActiveTopGroups() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("groupLedger", true))
				.add(Restrictions.eq("deletedYn", false)).add(Restrictions.eq("mainGroup", true)).list();
	}

	@Override
	public List<Groups> getAllLiabilityMainGroups() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("group.id", 4L)).list();
	}

	@Override
	public List<Groups> getAllActiveLedgers() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("groupLedger", false))
				.add(Restrictions.eq("deletedYn", false)).list();
	}

	@Override
	public Groups getGroupBasedOnName(String groupName) {
		return (Groups) getSession().createCriteria(Groups.class).add(Restrictions.eq("groupName", groupName))
				.uniqueResult();
	}

	@Override
	public Groups getGroupsById(Long groupId) {
		return (Groups) getSession().createCriteria(Groups.class).add(Restrictions.eq("id", groupId)).uniqueResult();
	}

	@Override
	public List<Groups> searchOnLedgerMasterData(String searchWord) {
		Criteria criteria = getSession().createCriteria(Groups.class);
		criteria.createAlias("ledgerMasterData", "lmd");
		Criterion rest1 = Restrictions.ilike("lmd.name", '%' + searchWord + '%');
		Criterion rest2 = Restrictions.like("lmd.address", '%' + searchWord + '%');
		Criterion rest3 = Restrictions.like("lmd.state", '%' + searchWord + '%');
		Criterion rest4 = Restrictions.like("lmd.pincode", '%' + searchWord + '%');
		Criterion rest5 = Restrictions.like("lmd.email", '%' + searchWord + '%');
		Criterion rest6 = Restrictions.like("lmd.phone", '%' + searchWord + '%');
		Criterion rest7 = Restrictions.like("lmd.aadhar", '%' + searchWord + '%');
		Criterion rest8 = Restrictions.like("lmd.pan", '%' + searchWord + '%');
		Criterion rest9 = Restrictions.like("lmd.gstNumber", '%' + searchWord + '%');
		Criterion rest10 = Restrictions.ilike("groupName", '%' + searchWord + '%');
		criteria.add(Restrictions.or(rest1, rest2, rest3, rest4, rest5, rest6, rest7, rest8, rest9, rest10));
		return (List<Groups>) criteria.list();
	}
	
	@Override
	public List<Groups> searchOnLedgerMasterDataOnlyGroups(String searchWord) {
		Criteria criteria = getSession().createCriteria(Groups.class);
		Criterion rest1 = Restrictions.ilike("groupName", '%' + searchWord + '%');
		criteria.add(Restrictions.or(rest1));
		return (List<Groups>) criteria.list();
	}

}
