package com.casoftware.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.User;
import com.casoftware.modal.UserCashbook;

@Repository("mainDao")
public class MainDaoImpl extends AbstractDao implements MainDao {

	@Override
	public List<User> getAllUsers() {
		Session session = getSession();
		return (List<User>) session.createCriteria(User.class).list();
	}

	@Override
	public List<UserCashbook> getAllActiveUsersCashbook() {
		return (List<UserCashbook>) getSession().createCriteria(UserCashbook.class).list();
	}

	@Override
	public List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDate(Long cashbookId, Date startDate,
			Date endDate) {
		return (List<BankTransactions>) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.eq("bank.id", cashbookId)).add(Restrictions.ne("approvedPending", 3))
				.add(Restrictions.between("transactionDate", startDate, endDate)).list();
	}

	@Override
	public List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDateStatus(Long cashbookId, Date startDate,
			Date endDate, int status) {
		return (List<BankTransactions>) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.eq("bank.id", cashbookId)).add(Restrictions.eq("approvedPending", status))
				.add(Restrictions.between("transactionDate", startDate, endDate)).list();
	}

	@Override
	public BankTransactions getBankTransactionBasedonBankTransactionId(Long bankTransactionId) {
		return (BankTransactions) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.eq("id", bankTransactionId)).uniqueResult();
	}

}
