package com.casoftware.dao;

import java.util.List;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.Group;
import com.casoftware.modal.Groups;
import com.casoftware.modal.UserCashbook;

@Repository("bankDao")
public class BankDaoImpl extends AbstractDao implements BankDao {

	@Override
	public Bank checkBankDetailsAvailableInDB(String bankName) {
		return (Bank) getSession().createCriteria(Bank.class).add(Restrictions.eq("bankName", bankName.trim()))
				.uniqueResult();
	}

	@Override
	public List<Bank> getAllBanks(Boolean bankCashbook) {
		return (List<Bank>) getSession().createCriteria(Bank.class).add(Restrictions.eq("bankCashbook", bankCashbook))
				.list();
	}

	@Override
	public List<Bank> getAllBanksCashbooks() {
		return (List<Bank>) getSession().createCriteria(Bank.class).addOrder(Order.asc("bankCashbook")).list();
	}

	@Override
	public List<BankTransactions> getTotalBankTransactionsinDBForBank(Long bankId) {
		return (List<BankTransactions>) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.eq("bank.id", bankId)).list();
	}

	@Override
	public UserCashbook getCashbookBasedOnUser(Long userId) {
		return (UserCashbook) getSession().createCriteria(UserCashbook.class).add(Restrictions.eq("deletedYn", false))
				.add(Restrictions.eq("user.id", userId)).uniqueResult();
	}

	@Override
	public List<Groups> getAllActiveCashbookLedgers() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("deletedYn", false))
				.add(Restrictions.eq("groupLedger", false)).add(Restrictions.eq("cashLedger", true)).list();
	}

	@Override
	public List<Groups> getAllActiveBankLedgers() {
		return (List<Groups>) getSession().createCriteria(Groups.class).add(Restrictions.eq("deletedYn", false))
				.add(Restrictions.eq("groupLedger", false)).add(Restrictions.eq("cashLedger", false)).list();
	}

	@Override
	public Bank getBankOrCashbookBasedOnId(Long id) {
		return (Bank) getSession().createCriteria(Bank.class).add(Restrictions.eq("id", id)).uniqueResult();
	}

}
