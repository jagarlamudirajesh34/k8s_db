package com.casoftware.dao;

import java.util.Date;
import java.util.List;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;

public interface BankTransactionsDao extends Dao {

	public List<BankTransactions> getAllBankTransactions();

	// for fetching bank based on bank id
	public Bank getBankBasedonId(Long bankId);

	// for fetching transaction based on transaction id
	public BankTransactions getTransactionBasedOnId(Long transactionId);

	public List<BankTransactions> getAllBankTransactionsForTableView(Date fromDate, Date toDate);
	
	public List<BankTransactions> getTransactionsBasedOnIntegerSearch(Integer searchWord);
	
	public List<BankTransactions> getTransactionsBasedOnStringSearch(String searchWord);
	

}
