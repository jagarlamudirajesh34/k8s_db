package com.casoftware.dao;

import java.util.List;

import com.casoftware.dto.LedgerDto;
import com.casoftware.modal.Groups;

public interface GroupLedgerDao extends Dao {

	public List<LedgerDto> getAllList();

	public List<Groups> getAllActiveGroups();

	public List<Groups> getAllActiveTopGroups();

	public List<Groups> getAllLiabilityMainGroups();

	// for getting all active ledger
	public List<Groups> getAllActiveLedgers();

	// for getting group based on group name
	public Groups getGroupBasedOnName(String groupName);

	// for getting group by id
	public Groups getGroupsById(Long groupId);

	// for search on ledgers and ledgers masetr data or info
	public List<Groups> searchOnLedgerMasterData(String searchWord);
	
	public List<Groups> searchOnLedgerMasterDataOnlyGroups(String searchWord);

}
