package com.casoftware.dao;

import java.util.Date;
import java.util.List;

import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.User;
import com.casoftware.modal.UserCashbook;

public interface MainDao extends Dao {

	List<User> getAllUsers();

	List<UserCashbook> getAllActiveUsersCashbook();

	List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDate(Long cashbookId, Date startDate,
			Date endDate);

	List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDateStatus(Long cashbookId, Date startDate,
			Date endDate, int status);
	
	BankTransactions getBankTransactionBasedonBankTransactionId(Long bankTransactionId);

}
