package com.casoftware.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;

@Repository("bankTransactionsDao")
public class BankTransactionsDaoImpl extends AbstractDao implements BankTransactionsDao {

	@Override
	public List<BankTransactions> getAllBankTransactions() {
		return (List<BankTransactions>) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.ne("approvedPending", 3)).add(Restrictions.eq("deletedYn", false))
				.addOrder(Order.desc("transactionDate")).list();
	}

	@Override
	public Bank getBankBasedonId(Long bankId) {
		return (Bank) getSession().createCriteria(Bank.class).add(Restrictions.eq("id", bankId)).uniqueResult();
	}

	@Override
	public BankTransactions getTransactionBasedOnId(Long transactionId) {
		return (BankTransactions) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.ne("approvedPending", 3)).add(Restrictions.eq("deletedYn", false))
				.add(Restrictions.eq("id", transactionId)).uniqueResult();
	}

	@Override
	public List<BankTransactions> getAllBankTransactionsForTableView(Date fromDate, Date toDate) {
		return (List<BankTransactions>) getSession().createCriteria(BankTransactions.class)
				.add(Restrictions.ne("approvedPending", 3)).add(Restrictions.eq("deletedYn", false))
				.add(Restrictions.between("transactionDate", fromDate, toDate)).addOrder(Order.desc("transactionDate"))
				.list();
	}

	@Override
	public List<BankTransactions> getTransactionsBasedOnIntegerSearch(Integer searchWord) {
		Criteria criteria = getSession().createCriteria(BankTransactions.class);
		//Criterion rest1 = Restrictions.like("balance", '%' + searchWord + '%');
		Criterion rest2 = Restrictions.eq("debit", searchWord);
		Criterion rest3 = Restrictions.eq("credit", searchWord);
		Criterion rest4 = Restrictions.eq("id", searchWord.longValue());
		criteria.add(Restrictions.or(rest2, rest3, rest4));
		return (List<BankTransactions>) criteria.list();
	}

	@Override
	public List<BankTransactions> getTransactionsBasedOnStringSearch(String searchWord) {
		Criteria criteria = getSession().createCriteria(BankTransactions.class);
		criteria.createAlias("bank", "bank");
		Criterion rest1 = Restrictions.like("bank.bankName", '%' + searchWord + '%');
		Criterion rest2 = Restrictions.like("userComments", '%' + searchWord + '%');
		Criterion rest3 = Restrictions.like("ledgerName", '%' + searchWord + '%');
		Criterion rest4 = Restrictions.like("creditLedgerName", '%' + searchWord + '%');
		Criterion rest5 = Restrictions.like("description", '%' + searchWord + '%');
		criteria.add(Restrictions.or(rest1, rest2, rest3, rest4, rest5));
		return (List<BankTransactions>) criteria.list();
	}

}
