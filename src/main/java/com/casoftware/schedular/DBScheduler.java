package com.casoftware.schedular;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.casoftware.modal.Bank;
import com.casoftware.service.AccessingFileFromDownloads;
import com.casoftware.service.BankService;

@Service
public class DBScheduler {

	@Autowired
	BankService bankService;

	AccessingFileFromDownloads affd = new AccessingFileFromDownloads();

	//@Scheduled(cron = "0/30 * * * * ?")
	public void startBankTransactionsUpdate() {
		System.err.println("Started");
		try {
			System.err.println("try block started");
			if (affd.checkFileExisted() != null) {
				System.out.println("File name = " + affd.checkFileExisted());
				System.out.println("------- Downloaded File Available -------");
				InputStream ExcelFileToRead = new FileInputStream(affd.checkFileExisted());
				HSSFWorkbook readingWorkbook = new HSSFWorkbook(ExcelFileToRead);
				HSSFSheet bankSheet = readingWorkbook.getSheetAt(0);
				System.out.println("Sheet Name = " + bankSheet.getSheetName());
				HSSFRow readingRow;
				HSSFCell readingCell;
				int rowCount = 0;
				Iterator rows = bankSheet.rowIterator();
				while (rows.hasNext()) {
					readingRow = (HSSFRow) rows.next();
					Iterator cells = readingRow.cellIterator();
					int cellCount = 0;
					Bank bankDetails = null;
					Bank bd = new Bank();
					while (cells.hasNext()) {
						readingCell = (HSSFCell) cells.next();
						if (readingCell.getCellType() == 1 && cellCount == 0 && rowCount > 2) {
							bankDetails = bankService.checkBankDetailsAvailableInDB(readingCell.getStringCellValue());
							System.err.println(bankDetails);
						}
						if (cellCount == 4 && rowCount > 2 && readingCell.getCellType() == 0 && bankDetails != null) {
							bankDetails.setBalance(readingCell.getNumericCellValue());
							bankService.saveOrUpdateBankDetails(bankDetails);
						} else if (rowCount > 2 && bankDetails == null) {
							if (cellCount == 0) {
								bd.setBankName(readingCell.getStringCellValue().trim());
							} else if (cellCount == 1) {
								bd.setType(readingCell.getStringCellValue());
							} else if (cellCount == 2) {
								bd.setInstitution(readingCell.getStringCellValue().trim());
							} else if (cellCount == 3) {
								// bd.setType(readingCell.getStringCellValue());
							} else if (cellCount == 4) {
								bd.setBalance(readingCell.getNumericCellValue());
							} else if (cellCount == 5) {
								bd.setLockedAmount(readingCell.getNumericCellValue());
							} else if (cellCount == 6) {
								bd.setOverDraft(readingCell.getNumericCellValue());
								bd.setBankCashbook(true);
								bankService.saveOrUpdateBankDetails(bd);
							}
						}
						cellCount++;
					}
					rowCount++;
				}
			} else {
				System.out.println("File name = " + affd.checkFileExisted());
				System.out.println("------- Downloaded File NOT Available -------");
			}
			System.err.println("Try block ended");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e);
		}
		System.err.println("Ended");
	}
}