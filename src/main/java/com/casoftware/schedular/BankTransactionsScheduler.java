package com.casoftware.schedular;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.service.AccessingFileFromDownloads;
import com.casoftware.service.BankService;
import com.casoftware.service.BankTransactionsService;

@Service
public class BankTransactionsScheduler {

	@Autowired
	BankTransactionsService bankTransactionsService;

	@Autowired
	BankService bankService;

	AccessingFileFromDownloads affd = new AccessingFileFromDownloads();

	//@Scheduled(cron = "0/30 * * * * ?")
	public void startBankTransactionsUpdate() {
		System.err.println("Started");
		try {
			System.out.println("---------------------------------------------");
			System.out.println("service started");
			if (affd.checkFileExisted() != null) {
				// System.out.println("download file available, file name = " +
				// affd.checkFileExisted());
				InputStream ExcelFileToRead = new FileInputStream(affd.checkFileExisted());
				HSSFWorkbook readingWorkbook = new HSSFWorkbook(ExcelFileToRead);
				int numberOfSheets = readingWorkbook.getNumberOfSheets();
				// System.out.println("numberOfSheets = " + numberOfSheets);
				for (int sheetNumber = 1; sheetNumber < numberOfSheets; sheetNumber++) {
					Bank bankDetails = bankService.checkBankDetailsAvailableInDB(
							readingWorkbook.getSheetAt(sheetNumber).getSheetName().replace("bank", "").trim());
					if (bankDetails != null && bankDetails.getId() != null) {
						System.out.println("Bank Details Found");
						System.out.println(readingWorkbook.getSheetAt(sheetNumber).getSheetName().replace("bank", "")
								+ " " + bankDetails.getBankName());
						List<BankTransactions> bankTransactionsCount = bankService
								.getTotalBankTransactionsinDBForBank(bankDetails.getId());
						System.err.println("count = " + bankTransactionsCount.size());
						HSSFSheet readingSheet = readingWorkbook.getSheetAt(sheetNumber);
						HSSFRow readingRow;
						HSSFCell readingCell;
						Iterator rows = readingSheet.rowIterator();
						int rowCount = 3;
						rowCount = rowCount + bankTransactionsCount.size();
						int readingLastRowNumber = readingSheet.getLastRowNum();
						readingLastRowNumber = readingLastRowNumber + 1;
						System.err.println(rowCount + " --- " + readingLastRowNumber);
						for (int i = rowCount; i < readingLastRowNumber; i++) {
							HSSFRow readingSheetRow1 = readingSheet.getRow(i);
							Iterator cells = readingSheetRow1.cellIterator();
							int cellCount = 0;
							BankTransactions bt = new BankTransactions();
							bt.setBank(bankDetails);
							while (cells.hasNext()) {
								readingCell = (HSSFCell) cells.next();
								if (cellCount == 1) {
									if (readingCell.getCellType() == 0) {
										bt.setTransactionDate(readingCell.getDateCellValue());
									}
								} else if (cellCount == 2) {
									bt.setDescription(readingCell.getStringCellValue().trim());
								} else if (cellCount == 3) {
									if (readingCell.getCellType() == 0) {
										int d = (int) readingCell.getNumericCellValue();
										int length = (int) (Math.log10(d) + 1);
										if (length == 1) {
											bt.setChequeNo("00000" + String.valueOf(d));
										} else if (length == 2) {
											bt.setChequeNo("0000" + String.valueOf(d));
										} else if (length == 3) {
											bt.setChequeNo("000" + String.valueOf(d));
										} else if (length == 4) {
											bt.setChequeNo("00" + String.valueOf(d));
										} else if (length == 5) {
											bt.setChequeNo("0" + String.valueOf(d));
										} else {
											bt.setChequeNo(String.valueOf(d));
										}
									}
								} else if (cellCount == 4) {
									bt.setDebit((int) readingCell.getNumericCellValue());
								} else if (cellCount == 5) {
									bt.setCredit((int) readingCell.getNumericCellValue());
								} else if (cellCount == 6) {
									bt.setBalance((int) readingCell.getNumericCellValue());
								} else if (cellCount == 7) {
									bt.setCategory(readingCell.getStringCellValue());
								} else if (cellCount == 8) {
									bt.setRemarks(readingCell.getStringCellValue());
								}
								cellCount++;
							}
							bt.setDeletedYn(false);
							bt.setCreatedDate(Calendar.getInstance().getTime());
							bt.setUpdatedDate(Calendar.getInstance().getTime());
							System.err.println(bt);
							bankTransactionsService.saveOrUpdateBankTransaction(bt);
						}

					}
				}
			} else {

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.err.println("Ended");
	}
}
