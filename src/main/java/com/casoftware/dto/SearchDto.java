package com.casoftware.dto;

public class SearchDto {

	private String searchWord;
	private String fromDate;
	private String toDate;

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	@Override
	public String toString() {
		return "SearchDto [searchWord=" + searchWord + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}

}
