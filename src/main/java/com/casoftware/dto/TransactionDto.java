package com.casoftware.dto;

import java.util.Date;

public class TransactionDto {

	public Long transactionId;
	public String bankName;
	public Date transactionDate;
	public String description;
	public String userComments;
	public String cheque;
	public int debit;
	public int credit;
	// public int outflow; // debit from perfios
	// public int inflow; // credit from perfios;
	public int balance;
	private int debitLedgerId;
	private String debitLedgerName;
	private int creditLedgerId;
	private String creditLedgerName;
	public Long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserComments() {
		return userComments;
	}
	public void setUserComments(String userComments) {
		this.userComments = userComments;
	}
	public String getCheque() {
		return cheque;
	}
	public void setCheque(String cheque) {
		this.cheque = cheque;
	}
	public int getDebit() {
		return debit;
	}
	public void setDebit(int debit) {
		this.debit = debit;
	}
	public int getCredit() {
		return credit;
	}
	public void setCredit(int credit) {
		this.credit = credit;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public int getDebitLedgerId() {
		return debitLedgerId;
	}
	public void setDebitLedgerId(int debitLedgerId) {
		this.debitLedgerId = debitLedgerId;
	}
	public String getDebitLedgerName() {
		return debitLedgerName;
	}
	public void setDebitLedgerName(String debitLedgerName) {
		this.debitLedgerName = debitLedgerName;
	}
	public int getCreditLedgerId() {
		return creditLedgerId;
	}
	public void setCreditLedgerId(int creditLedgerId) {
		this.creditLedgerId = creditLedgerId;
	}
	public String getCreditLedgerName() {
		return creditLedgerName;
	}
	public void setCreditLedgerName(String creditLedgerName) {
		this.creditLedgerName = creditLedgerName;
	}
	@Override
	public String toString() {
		return "TransactionDto [transactionId=" + transactionId + ", bankName=" + bankName + ", transactionDate="
				+ transactionDate + ", description=" + description + ", userComments=" + userComments + ", cheque="
				+ cheque + ", debit=" + debit + ", credit=" + credit + ", balance=" + balance + ", debitLedgerId="
				+ debitLedgerId + ", debitLedgerName=" + debitLedgerName + ", creditLedgerId=" + creditLedgerId
				+ ", creditLedgerName=" + creditLedgerName + "]";
	}

}