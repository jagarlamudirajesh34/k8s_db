package com.casoftware.dto;

import java.util.List;

import com.casoftware.modal.LedgerMasterData;

public class LedgerDto {

	private Long id;
	private String groupName;
	private List<LedgerDto> ledgerDtoList;
	private String groupHierarchyName;
	private String ledgerMasterInfo;
	private LedgerMasterData ledgerMasterData;
	
	public String getLedgerMasterInfo() {
		return ledgerMasterInfo;
	}

	public void setLedgerMasterInfo(String ledgerMasterInfo) {
		this.ledgerMasterInfo = ledgerMasterInfo;
	}

	public String getGroupHierarchyName() {
		return groupHierarchyName;
	}

	public void setGroupHierarchyName(String groupHierarchyName) {
		this.groupHierarchyName = groupHierarchyName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<LedgerDto> getLedgerDtoList() {
		return ledgerDtoList;
	}

	public void setLedgerDtoList(List<LedgerDto> ledgerDtoList) {
		this.ledgerDtoList = ledgerDtoList;
	}

	public LedgerMasterData getLedgerMasterData() {
		return ledgerMasterData;
	}

	public void setLedgerMasterData(LedgerMasterData ledgerMasterData) {
		this.ledgerMasterData = ledgerMasterData;
	}

	@Override
	public String toString() {
		return "LedgerDto [id=" + id + ", groupName=" + groupName + ", ledgerDtoList=" + ledgerDtoList
				+ ", groupHierarchyName=" + groupHierarchyName + ", ledgerMasterInfo=" + ledgerMasterInfo
				+ ", ledgerMasterData=" + ledgerMasterData + "]";
	}

}
