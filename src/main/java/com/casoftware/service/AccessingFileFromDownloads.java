package com.casoftware.service;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Calendar;
import java.util.Date;

public class AccessingFileFromDownloads {

	public static final String directory = "C:/Users/jagar/Downloads/";
	
	public String checkFileExisted() {
		String fileName = null;
		try {
			File dir = new File(directory);
			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.startsWith(formFileName());
				}
			};
			String[] fileAvailables = dir.list(filter);
			if (fileAvailables != null && fileAvailables.length > 0) {
				fileName = directory+fileAvailables[fileAvailables.length - 1];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
	
	public static String formFileName() {
		Date date = Calendar.getInstance().getTime();
		int dat = date.getDate();
		int month = date.getMonth() + 1;
		int year = 2018;
		if (date.getYear() == 119) {
			year = 2019;
		} else if (date.getYear() == 120) {
			year = 2020;
		} else if (date.getYear() == 121) {
			year = 2021;
		}
		if (month < 10) {
			if(dat < 10){
				return "X" + year + "0" + month + "0" + dat;
			}else{
				return "X" + year + "0" + month + "" + dat;
			}
		} else {
			if(dat < 10){
				return "X" + year + "" + month + "0" + dat;
			}else{
				return "X" + year + "" + month + "" + dat;
			}
		}
	}
	
}
