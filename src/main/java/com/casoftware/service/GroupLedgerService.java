package com.casoftware.service;

import java.util.List;

import com.casoftware.dto.LedgerDto;
import com.casoftware.modal.Groups;

public interface GroupLedgerService {

	public List<LedgerDto> getAllList();

	public List<Groups> getAllActiveGroups();

	public List<Groups> getAllActiveTopGroups();

	public List<Groups> getAllLiabilityMainGroups();

	public List<Groups> getSubGroups(Long groupId);

	// for getting all active ledger
	public List<Groups> getAllActiveLedgers();

	// for editing ledgers master data
	public void editLedgerMasterData(Groups group);

	// for search on ledgers and ledgers master data
	public List<Groups> searchOnLedgerMasterData(String searchWord);

}
