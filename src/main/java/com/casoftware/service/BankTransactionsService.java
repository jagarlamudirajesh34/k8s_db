package com.casoftware.service;

import java.util.Date;
import java.util.List;

import com.casoftware.dto.TransactionDto;
import com.casoftware.modal.BankTransactions;

public interface BankTransactionsService {

	public List<BankTransactions> getAllBankTransactions();

	// for fetch all transactions for UI Grid
	public List<BankTransactions> getAllBankTransactionsForTableView();

	// for fetch all transactions for UI Grid
	public List<BankTransactions> getAllBankTransactionsForTableView(Date fromDate, Date toDate);

	public void saveOrUpdateBankTransaction(BankTransactions bt);

	public void saveBankTransaction(BankTransactions bankTransactions);

	public void editBankTransaction(BankTransactions bankTransactions);

	// for edit transaction from UI Grid view
	public void editTransactionFromUiGrid(TransactionDto transactionDto);

	// for get search on ui grid table
	public List<BankTransactions> getTransactionsBasedOnSearch(String searchWord);
	
	//for update ledger info in bank transactions
	public void updateLedgersInTransactioninUiGrid(TransactionDto transactionDto);

}
