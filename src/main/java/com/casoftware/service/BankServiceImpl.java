package com.casoftware.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casoftware.dao.BankDao;
import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.Groups;
import com.casoftware.modal.UserCashbook;

@Service("bankService")
@Transactional("transactionManager")
public class BankServiceImpl implements BankService {

	@Autowired
	BankDao bankDao;

	@Override
	public Bank checkBankDetailsAvailableInDB(String bankName) {
		return bankDao.checkBankDetailsAvailableInDB(bankName);
	}

	@Override
	public void saveOrUpdateBankDetails(Bank bank) {
		bank.setLastUpdated(Calendar.getInstance().getTime());
		bankDao.saveOrUpdate(bank);
	}

	@Override
	public List<Bank> getAllBanks(Boolean bankCashbook) {
		return bankDao.getAllBanks(bankCashbook);
	}

	@Override
	public List<Bank> getAllBanksCashbooks() {
		return bankDao.getAllBanksCashbooks();
	}

	@Override
	public List<BankTransactions> getTotalBankTransactionsinDBForBank(Long bankId) {
		return bankDao.getTotalBankTransactionsinDBForBank(bankId);
	}

	@Override
	public UserCashbook getCashbookBasedOnUser(Long userId) {
		return bankDao.getCashbookBasedOnUser(userId);
	}

	@Override
	public List<Groups> getAllActiveCashbookLedgers() {
		return bankDao.getAllActiveCashbookLedgers();
	}
	
	@Override
	public List<Groups> getAllActiveBankLedgers() {
		return bankDao.getAllActiveBankLedgers();
	}

	@Override
	public Bank getBankOrCashbookBasedOnId(Long id) {
		return bankDao.getBankOrCashbookBasedOnId(id);
	}

}
