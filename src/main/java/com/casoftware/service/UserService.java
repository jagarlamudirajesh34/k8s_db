package com.casoftware.service;

import com.casoftware.modal.User;

public interface UserService {

	User getUser(String email);

}
