package com.casoftware.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casoftware.dao.BankDao;
import com.casoftware.dao.MainDao;
import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.User;
import com.casoftware.modal.UserCashbook;

@Service("mainService")
@Transactional("transactionManager")
public class MainServiceImpl implements MainService {

	@Autowired
	MainDao mainDao;

	@Autowired
	BankDao bankDao;

	@Override
	public List<User> getAllUsers() {
		return mainDao.getAllUsers();
	}

	@Override
	public List<UserCashbook> getAllActiveUsersCashbook() {
		return mainDao.getAllActiveUsersCashbook();
	}

	@Override
	public List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookId(Long cashbookId) {
		return bankDao.getTotalBankTransactionsinDBForBank(cashbookId);
	}

	@Override
	public List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDateStatus(Long cashbookId, Date startDate,
			Date endDate, String status) {
		if (status.equals("all")) {
			return mainDao.getAllCashbookTransactionsBasedOnCashbookIdDate(cashbookId, startDate, endDate);
		} else if (status.equals("approved")) {
			return mainDao.getAllCashbookTransactionsBasedOnCashbookIdDateStatus(cashbookId, startDate, endDate, 1);
		} else if (status.equals("pending")) {
			return mainDao.getAllCashbookTransactionsBasedOnCashbookIdDateStatus(cashbookId, startDate, endDate, 0);
		} else if (status.equals("rejected")) {
			return mainDao.getAllCashbookTransactionsBasedOnCashbookIdDateStatus(cashbookId, startDate, endDate, 2);
		}
		return null;
	}

	@Override
	public void approveRejectTransaction(BankTransactions bankTransaction) {
		BankTransactions btDb = mainDao.getBankTransactionBasedonBankTransactionId(bankTransaction.getId());
		btDb.setAdminComments(bankTransaction.getAdminComments());
		btDb.setUserComments(bankTransaction.getUserComments());
		btDb.setApprovedPending(bankTransaction.getApprovedPending());
		mainDao.saveOrUpdate(btDb);
	}

}
