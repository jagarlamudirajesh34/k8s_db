package com.casoftware.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casoftware.dao.GroupLedgerDao;
import com.casoftware.dto.LedgerDto;
import com.casoftware.modal.Groups;
import com.casoftware.modal.LedgerMasterData;

@Service("groupLedgerService")
@Transactional("transactionManager")
public class GroupLedgerServiceImpl implements GroupLedgerService {

	@Autowired
	GroupLedgerDao groupLedgerDao;

	@Override
	public List<LedgerDto> getAllList() {
		return groupLedgerDao.getAllList();
	}

	@Override
	public List<Groups> getAllActiveGroups() {
		return groupLedgerDao.getAllActiveGroups();
	}

	@Override
	public List<Groups> getAllActiveTopGroups() {
		return groupLedgerDao.getAllActiveTopGroups();
	}

	@Override
	public List<Groups> getAllLiabilityMainGroups() {
		return groupLedgerDao.getAllLiabilityMainGroups();
	}

	@Override
	public List<Groups> getSubGroups(Long groupId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Groups> getAllActiveLedgers() {
		return groupLedgerDao.getAllActiveLedgers();
	}

	@Override
	public void editLedgerMasterData(Groups group) {
		LedgerMasterData lmd = group.getLedgerMasterData();
		if (lmd.getAddress() == null || lmd.getAddress().equals("")) {
			lmd.setAddress(null);
		}
		if (lmd.getPan() == null || lmd.getPan().equals("")) {
			lmd.setPan(null);
		}
		if (lmd.getPincode() == null || lmd.getPincode().equals("")) {
			lmd.setPincode(null);
		}
		if (lmd.getState() == null || lmd.getState().equals("")) {
			lmd.setState(null);
		}
		if (lmd.getPhone() == null || lmd.getPhone().equals("")) {
			lmd.setPhone(null);
		}
		if (lmd.getEmail() == null || lmd.getEmail().equals("")) {
			lmd.setEmail(null);
		}
		if (lmd.getAadhar() == null || lmd.getAadhar().equals("")) {
			lmd.setAadhar(null);
		}
		if (lmd.getGstNumber() == null || lmd.getGstNumber().equals("")) {
			lmd.setGstNumber(null);
		}
		groupLedgerDao.saveOrUpdate(lmd);
		Groups groups = groupLedgerDao.getGroupsById(group.getId());
		groups.setLedgerMasterData(lmd);
	}

	@Override
	public List<Groups> searchOnLedgerMasterData(String searchWord) {
		List<Groups> allGroups = groupLedgerDao.searchOnLedgerMasterData(searchWord);
		Set<Groups> set = new HashSet<Groups>();
		set.addAll(allGroups);
		List<Groups> allGroups1 = groupLedgerDao.searchOnLedgerMasterDataOnlyGroups(searchWord);
		set.addAll(allGroups1);
		List<Groups> returnGroups = new ArrayList<Groups>(set);
		return returnGroups;
	}

}
