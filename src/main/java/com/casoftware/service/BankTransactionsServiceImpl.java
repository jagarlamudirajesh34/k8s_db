package com.casoftware.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.casoftware.dao.BankTransactionsDao;
import com.casoftware.dao.GroupLedgerDao;
import com.casoftware.dto.TransactionDto;
import com.casoftware.modal.Bank;
import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.Groups;

@Service("bankTransactionsService")
@Transactional("transactionManager")
public class BankTransactionsServiceImpl implements BankTransactionsService {

	@Autowired
	BankTransactionsDao bankTransactionsDao;

	@Autowired
	GroupLedgerDao groupLedgerDao;

	@Override
	public List<BankTransactions> getAllBankTransactions() {
		return bankTransactionsDao.getAllBankTransactions();
	}

	@Override
	public void saveOrUpdateBankTransaction(BankTransactions bt) {
		Groups group = groupLedgerDao.getGroupBasedOnName(bt.getBank().getBankName().trim());
		if (bt.getCredit() == 0) {
			bt.setCreditLedgerId(group.getId().intValue());
			bt.setCreditLedgerName(bt.getBank().getBankName().trim());
			bt.setLedgerId(42);
			bt.setLedgerName("Suspense");
		}
		if (bt.getDebit() == 0) {
			bt.setLedgerId(group.getId().intValue());
			bt.setLedgerName(bt.getBank().getBankName());
			bt.setCreditLedgerId(42);
			bt.setCreditLedgerName("Suspense");
		}
		bankTransactionsDao.saveOrUpdate(bt);
	}

	@Override
	public void saveBankTransaction(BankTransactions bankTransactions) {
		Bank bank = bankTransactionsDao.getBankBasedonId(bankTransactions.getBank().getId());
		if (bankTransactions.getCredit() == 0) {
			bank.setBalance(bank.getBalance() - bankTransactions.getDebit());
		} else {
			bank.setBalance(bank.getBalance() + bankTransactions.getCredit());
		}
		bankTransactions.setBalance(bank.getBalance().intValue());
		bankTransactionsDao.saveOrUpdate(bankTransactions);
		bankTransactionsDao.saveOrUpdate(bank);
	}

	@Override
	public void editBankTransaction(BankTransactions bankTransactions) {
		bankTransactions.setUpdatedDate(Calendar.getInstance().getTime());
		bankTransactionsDao.saveOrUpdate(bankTransactions);
	}

	@Override
	public List<BankTransactions> getAllBankTransactionsForTableView() {
		return bankTransactionsDao.getAllBankTransactions();
	}

	@Override
	public void editTransactionFromUiGrid(TransactionDto transactionDto) {
		BankTransactions btDB = bankTransactionsDao.getTransactionBasedOnId(transactionDto.getTransactionId());
		btDB.setDescription(transactionDto.getDescription());
		btDB.setUserComments(transactionDto.getUserComments());
		bankTransactionsDao.saveOrUpdate(btDB);
	}

	@Override
	public List<BankTransactions> getAllBankTransactionsForTableView(Date fromDate, Date toDate) {
		return bankTransactionsDao.getAllBankTransactionsForTableView(fromDate, toDate);
	}

	@Override
	public List<BankTransactions> getTransactionsBasedOnSearch(String searchWord) {
		try {
			return bankTransactionsDao.getTransactionsBasedOnIntegerSearch(Integer.parseInt(searchWord));
		} catch (Exception e) {
			return bankTransactionsDao.getTransactionsBasedOnStringSearch(searchWord);
		}
	}

	@Override
	public void updateLedgersInTransactioninUiGrid(TransactionDto transactionDto) {
		BankTransactions bt = bankTransactionsDao.getTransactionBasedOnId(transactionDto.getTransactionId());
		if (bt.getCredit() == 0) {
			Groups g = groupLedgerDao.getGroupBasedOnName(transactionDto.getDebitLedgerName());
			if (g != null) {
				bt.setLedgerId(g.getId().intValue());
				bt.setLedgerName(transactionDto.getDebitLedgerName());
			}
		} 
		if (bt.getDebit() == 0) {
			Groups g = groupLedgerDao.getGroupBasedOnName(transactionDto.getCreditLedgerName());
			if (g != null) {
				bt.setCreditLedgerId(g.getId().intValue());
				bt.setCreditLedgerName(transactionDto.getCreditLedgerName());
			}

		}
		bankTransactionsDao.saveOrUpdate(bt);
	}

}
