package com.casoftware.service;

import java.util.Date;
import java.util.List;

import com.casoftware.modal.BankTransactions;
import com.casoftware.modal.User;
import com.casoftware.modal.UserCashbook;

public interface MainService {

	List<User> getAllUsers();

	List<UserCashbook> getAllActiveUsersCashbook();

	List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookId(Long cashbookId);

	List<BankTransactions> getAllCashbookTransactionsBasedOnCashbookIdDateStatus(Long cashbookId, Date startDate,
			Date endDate, String status);

	public void approveRejectTransaction(BankTransactions bankTransaction);

}
